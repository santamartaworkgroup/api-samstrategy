import { Entity, Column, Index, OneToMany, PrimaryGeneratedColumn } from "typeorm";

import { ObjectType, Field, InputType } from "type-graphql";

import { BaseEntityImpl } from "../../infra/entity/base-entity-impl";

import { Processo } from './processo';

@ObjectType({  description: 'Analise de Produto Concorrente'})
@InputType(`${Analise.name}Arg`)

@Entity('ANALISE')

@Index(["nome"])
export class Analise extends BaseEntityImpl<Analise, string> {

    @Field({ nullable: true })
    @PrimaryGeneratedColumn("uuid", { name: 'ID' })    
    id: string;

    @Field({ nullable: true, description: "Nome da Análise" })
    @Column({ name: 'NOME', length: 50 })
    nome: string;

    @Field({ nullable: true, description: "Descrição detalhada da análise" })
    @Column({ name: 'DESCRICAO', nullable: true, length: 4000 })
    descricao: string;

    @Field({ nullable: true, description: "Data da geração da Análise" })
    @Column({ name: 'DATA' })
    data: Date = new Date();

    @Field(type => [Processo], { nullable: true, description: "Processos da Análise" })
    @OneToMany(type => Processo, processo => processo.analise, { cascade: ['insert', 'update'], eager: true })
    processos: Processo[];

}
