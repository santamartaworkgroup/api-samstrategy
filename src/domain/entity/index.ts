import { registerEnumType } from "type-graphql";

import { Concorrente } from './type/concorrente';
import { StatusProcesso } from './type/status-processo';

export function registerEntities(): void {
    registerEnumType(Concorrente, { name: "Concorrente", description: "Concorrentes da Analise de Produtos" });
    registerEnumType(StatusProcesso, { name: "StatusProcesso", description: "Status de Processos de Extração" });
};

