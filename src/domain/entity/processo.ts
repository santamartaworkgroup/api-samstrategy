import { Entity, Column, ManyToOne, Index, PrimaryGeneratedColumn, JoinColumn } from "typeorm";
import { ObjectType, Field, InputType } from "type-graphql";

import { BaseEntityImpl } from "../../infra/entity/base-entity-impl";

import { Concorrente } from "./type/concorrente";
import { StatusProcesso } from "./type/status-processo";
import { Analise } from "./analise";

@ObjectType({  description: 'Processo de extração de dados do Concorrente'})
@InputType(`${Processo.name}Arg`)

@Entity('PROCESSO')

@Index(["analise", "concorrente", "status", "data"])
export class Processo extends BaseEntityImpl<Processo, string> {

    @Field({ nullable: true })
    @PrimaryGeneratedColumn("uuid", { name: 'ID' })    
    id: string;

	@Field(type => Analise, { nullable: true })
    @ManyToOne(type => Analise, analise => analise.processos )
    @JoinColumn({ name: "ANALISE_ID" })    
	analise: Analise;

	@Field(type => Concorrente, { nullable: true })
	@Column({ name: 'CONCORRENTE'}) 
    concorrente: Concorrente;

    @Field({ nullable: true, description: "Data da criação do processo" })
    @Column({ name: 'DATA'})
    data: Date = new Date();

    @Field({ nullable: true, description: "Inicio do processamento do concorrente" })
    @Column({ name: 'INICIO', nullable: true })
    inicio: Date;

    @Field({ nullable: true, description: "Termino do processamento do concorrente" })
    @Column({ name: 'TERMINO', nullable: true })
    termino: Date;

	@Field(type => StatusProcesso, { nullable: true })
	@Column({ name: 'STATUS' }) 
    status: StatusProcesso = StatusProcesso.Criado;

	@Field({ nullable: true })
	@Column({ name: 'TOTAL_PROCESSADO', nullable: true })
    totalProcessado: number = 0;
    
    @Field({ nullable: true })
    @Column({ name: 'ERRO', nullable: true, length: 4000 })
    erro: string;

}
