import { Entity, Column, ManyToOne, Index, PrimaryGeneratedColumn, JoinColumn } from 'typeorm';

import { ObjectType, Field, InputType } from 'type-graphql';

import { BaseEntityImpl } from '../../infra/entity/base-entity-impl';

import { Concorrente } from './type/concorrente';
import { Analise } from './analise';
import { Processo } from './processo';

@ObjectType({ description: 'Produto extraido do Concorrente'})
@InputType(`${Produto.name}Arg`)

@Entity('PRODUTO')

@Index(["analise", "concorrente", "sessao", "categoria", "subcategoria"])
@Index(["analise", "concorrente", "categoria"])
@Index(["analise", "concorrente", "subcategoria"])
@Index(["analise", "concorrente", "ean"])
@Index(["analise", "concorrente", "nome"])
@Index(["analise", "concorrente", "marca"])
@Index(["analise", "concorrente", "fabricante"])
@Index(["analise", "concorrente", "vantagens"])
@Index(["analise", "concorrente", "precoRegular"])
@Index(["analise", "concorrente", "precoFinal"])
@Index(["processo"])
export class Produto extends BaseEntityImpl<Produto, string> {

    @Field({ nullable: true })
    @PrimaryGeneratedColumn("uuid", { name: 'ID' })    
    id: string;

	@Field(type => Analise, { nullable: true })
	@ManyToOne(type => Analise)
    @JoinColumn({ name: "ANALISE_ID" })    
	analise: Analise;

	@Field(type => Processo, { nullable: true })
	@ManyToOne(type => Processo)
    @JoinColumn({ name: "PROCESSO_ID" })    
	processo: Processo;

	@Field(type => Concorrente, { nullable: true })
	@Column({ name: 'CONCORRENTE' })
	concorrente: Concorrente;

	@Field({ nullable: true })
	@Column({ name: 'SESSAO', nullable: true, length: 50 }) 
	sessao: string;

	@Field({ nullable: true })
	@Column({ name: 'CATEGORIA', nullable: true, length: 50 }) 
	categoria: string;

	@Field({ nullable: true })
	@Column({ name: 'SUBCATEGORIA', nullable: true, length: 50 }) 
	subcategoria: string;

	@Field({ nullable: true })
	@Column({ name: 'SKU', nullable: true, length: 50 }) 
	sku: string;

	@Field({ nullable: true })
	@Column({ name: 'NOME', nullable: true, length: 250  }) 
	nome: string;
	
	@Field({ nullable: true })
	@Column({ name: 'DESCRICAO', nullable: true, length: 4000 }) 
	descricao: string;

	@Field({ nullable: true })
	@Column({ name: 'EAN', nullable: true, length: 50 }) 
	ean: string;

	@Field({ nullable: true })
	@Column({ name: 'PESO', nullable: true, length: 250 }) 
	peso: string;

	@Field({ nullable: true })
	@Column({ name: 'PRECO_REGULAR', nullable: true, type: 'decimal', precision: 14, scale: 2 })
	precoRegular: number;

	@Field({ nullable: true })
	@Column({ name: 'PRECO_FINAL', nullable: true, type: 'decimal', precision: 14, scale: 2 })
	precoFinal: number;

	@Field({ nullable: true })
	@Column({ name: 'VANTAGENS', nullable: true, length: 250 }) 
	vantagens: string;

	@Field({ nullable: true })
	@Column({ name: 'QUANTIDADE', nullable: true, length: 250 }) 
	quantidade: string;

	@Field({ nullable: true })
	@Column({ name: 'MARCA', nullable: true, length: 250 }) 
	marca: string;

	@Field({ nullable: true })
	@Column({ name: 'FABRICANTE', nullable: true, length: 500 }) 
	fabricante: string;

	@Field({ nullable: true })
	@Column({ name: 'LINK', nullable: true, length: 4000 }) 
	link: string;

	@Field({ nullable: true })
	@Column({ name: 'OUTROS', nullable: true, length: 4000 }) 
	outros: string;

	@Field({ nullable: true, description: "Data da última atualização" })
    @Column({ name: 'ULTIMA_ATUALIZACAO' })
    ultimaAtualizacao: Date = new Date();
	
}