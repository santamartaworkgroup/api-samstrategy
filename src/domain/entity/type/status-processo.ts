export enum StatusProcesso {
    Criado = 'Criado',
    Processar = 'Processar',
    Processando = 'Processando',
    Concluido = 'Concluido',
    Falhado = 'Falhado',
    Conciliando = 'Conciliando'
}