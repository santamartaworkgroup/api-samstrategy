import { Entity, Column, Index, PrimaryGeneratedColumn } from "typeorm";
import { ObjectType, Field, InputType } from "type-graphql";

import { BaseEntity } from "../../infra/entity/base-entity";
import { BaseEntityImpl } from "../../infra/entity/base-entity-impl";

@ObjectType({  description: 'Analise de Produto Concorrente'})
@InputType(`${Usuario.name}Arg`)

@Entity('USUARIO')

@Index(["login", "senha"], { unique: true })
@Index(["nome"])
export class Usuario extends BaseEntityImpl<Usuario, string> {

    @Field({ nullable: true })
    @PrimaryGeneratedColumn("uuid", { name: 'ID' })
    id: string;
    
    @Field({ nullable: true, description: "Login do Usuário" })
    @Column({ name: 'LOGIN', length: 50 })
    login: string;

    @Field({ nullable: true, description: "Senha do Usuário" })
    @Column({ name: 'SENHA', length: 50 })
    senha: string;

    @Field({ nullable: true, description: "Nome do Usuário" })
    @Column({ name: 'NOME', length: 70 })
    nome: string;

    @Field({ nullable: true, description: "Email do Usuário" })
    @Column({ name: 'EMAIL', nullable: true, length: 100 })
    email: string;

}
