import { BaseRepository } from '../../infra/repository/base-repository';

import { Analise } from '../entity/analise';

export interface AnaliseRepository extends BaseRepository<Analise, string> {
    
}