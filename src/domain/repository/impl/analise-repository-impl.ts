import { Service, Inject } from "typedi";

import { BaseRepositoryImpl } from "../../../infra/repository/base-repository-impl";

import { Analise } from "../../entity/analise";
import { AnaliseRepository } from '../analise-repository';
import { ProcessoRepository } from "../processo-repository";

@Service('analiseRepository')
export class AnaliseRepositoryImpl extends BaseRepositoryImpl(Analise)<Analise, string> implements AnaliseRepository {
     
}
