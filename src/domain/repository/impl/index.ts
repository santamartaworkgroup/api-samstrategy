import Container from 'typedi';

import { UsuarioRepositoryImpl } from './usuario-repository-impl';
import { AnaliseRepositoryImpl } from "./analise-repository-impl";
import { ProcessoRepositoryImpl } from './processo-repository-impl';
import { ProdutoRepositoryImpl } from './produto-repository-impl';

export function registerRepositories(): void {
    Container.get(UsuarioRepositoryImpl);
    Container.get(AnaliseRepositoryImpl);
    Container.get(ProcessoRepositoryImpl);
    Container.get(ProdutoRepositoryImpl);
}