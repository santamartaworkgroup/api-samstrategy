import { ProdutoRepository } from './../produto-repository';
import { Service, Inject } from "typedi";

import { BaseRepositoryImpl } from "../../../infra/repository/base-repository-impl";

import { Processo } from './../../entity/processo';
import { ProcessoRepository } from './../processo-repository';

@Service('processoRepository')
export class ProcessoRepositoryImpl extends BaseRepositoryImpl(Processo)<Processo, string> implements ProcessoRepository {

}
