import { Service } from 'typedi';

import { BaseRepositoryImpl } from '../../../infra/repository/base-repository-impl';

import { Produto } from '../../entity/produto';
import { ProdutoRepository } from '../produto-repository';
import { Brackets } from 'typeorm';
import { Concorrente } from '../../entity/type/concorrente';

@Service('produtoRepository')
export class ProdutoRepositoryImpl extends BaseRepositoryImpl(Produto)<Produto, string> implements ProdutoRepository {

    async consultarConcorrencia(filtro: any): Promise<[any[], number]> {
        if (!filtro['concorrentes'] || filtro['concorrentes'].length == 0) {
            throw new Error('Concorrente não informado');
        }

        let query = this.manager.createQueryBuilder(Produto, 'p0');

        for (let i = 1; i < filtro['concorrentes'].length; i++) {
            if (filtro['concorrentes'][i].filtrar) {
                query = query.innerJoinAndSelect(Produto, `p${i}`, `p${i}.ean = p0.ean and p${i}.analise.id = :analiseId and p${i}.concorrente = :concorrente${i}`);
            } else {
                query = query.leftJoinAndSelect(Produto, `p${i}`, `p${i}.ean = p0.ean and p${i}.analise.id = :analiseId and p${i}.concorrente = :concorrente${i}`);
            }
            query.setParameter(`concorrente${i}`, filtro['concorrentes'][i].nome);
        }

        query = query.andWhere(`p0.analise.id = :analiseId`);
        query = query.andWhere(`p0.concorrente = :concorrente0`);

        query.setParameter('analiseId', filtro['analiseId']);
        query.setParameter(`concorrente0`, filtro['concorrentes'][0].nome);

        if (filtro['nome']) {
            let qb = new Brackets(qb => {
                qb.where('p0.nome like :nome');
                for (let i = 1; i < filtro['concorrentes'].length; i++) {
                    qb = qb.orWhere(`p${i}.nome like :nome`);
                }
            });
            query.andWhere(qb);
            query.setParameter('nome', (filtro['nome'] as string).toUpperCase());
        }

        if (filtro['nomeNotNull']) {
            let qb = new Brackets(qb => {
                qb.where('p0.nome is not null');
                for (let i = 1; i < filtro['concorrentes'].length; i++) {
                    qb = qb.orWhere(`p${i}.nome is not null`);
                }
            });
            query.andWhere(qb);
        }

        
        if (filtro['sessao']) {
            let qb = new Brackets(qb => {
                qb.where('p0.sessao like :sessao');
                for (let i = 1; i < filtro['concorrentes'].length; i++) {
                    qb = qb.orWhere(`p${i}.sessao like :sessao`);
                }
            });
            query.andWhere(qb);
            query.setParameter('sessao', (filtro['sessao'] as string).toUpperCase());
        }

        if (filtro['sessaoNotNull']) {
            let qb = new Brackets(qb => {
                qb.where('p0.sessao is not null');
                for (let i = 1; i < filtro['concorrentes'].length; i++) {
                    qb = qb.orWhere(`p${i}.sessao is not null`);
                }
            });
            query.andWhere(qb);
        }
        
        if (filtro['categoria']) {
            let qb = new Brackets(qb => {
                qb.where('p0.categoria like :categoria');
                for (let i = 1; i < filtro['concorrentes'].length; i++) {
                    qb = qb.orWhere(`p${i}.categoria like :categoria`);
                }
            });
            query.andWhere(qb);
            query.setParameter('categoria', (filtro['categoria'] as string).toUpperCase());
        }

        if (filtro['categoriaNotNull']) {
            let qb = new Brackets(qb => {
                qb.where('p0.categoria is not null');
                for (let i = 1; i < filtro['concorrentes'].length; i++) {
                    qb = qb.orWhere(`p${i}.categoria is not null`);
                }
            });
            query.andWhere(qb);
        }

        if (filtro['subcategoria']) {
            let qb = new Brackets(qb => {
                qb.where('p0.subcategoria like :subcategoria');
                for (let i = 1; i < filtro['concorrentes'].length; i++) {
                    qb = qb.orWhere(`p${i}.subcategoria like :subcategoria`);
                }
            });
            query.andWhere(qb);
            query.setParameter('subcategoria', (filtro['subcategoria'] as string).toUpperCase());
        }

        if (filtro['subcategoriaNotNull']) {
            let qb = new Brackets(qb => {
                qb.where('p0.subcategoria is not null');
                for (let i = 1; i < filtro['concorrentes'].length; i++) {
                    qb = qb.orWhere(`p${i}.subcategoria is not null`);
                }
            });
            query.andWhere(qb);
        }

        if (filtro['marca']) {
            let qb = new Brackets(qb => {
                qb.where('p0.marca like :marca');
                for (let i = 1; i < filtro['concorrentes'].length; i++) {
                    qb = qb.orWhere(`p${i}.marca like :marca`);
                }
            });
            query.andWhere(qb);
            query.setParameter('marca', (filtro['marca'] as string).toUpperCase());
        }

        if (filtro['marcaNotNull']) {
            let qb = new Brackets(qb => {
                qb.where('p0.marca is not null');
                for (let i = 1; i < filtro['concorrentes'].length; i++) {
                    qb = qb.orWhere(`p${i}.marca is not null`);
                }
            });
            query.andWhere(qb);
        }

        if (filtro['fabricante']) {
            let qb = new Brackets(qb => {
                qb.where('p0.fabricante like :fabricante');
                for (let i = 1; i < filtro['concorrentes'].length; i++) {
                    qb = qb.orWhere(`p${i}.fabricante like :fabricante`);
                }
            });
            query.andWhere(qb);
            query.setParameter('fabricante', (filtro['fabricante'] as string).toUpperCase());
        }

        if (filtro['fabricanteNotNull']) {
            let qb = new Brackets(qb => {
                qb.where('p0.fabricante is not null');
                for (let i = 1; i < filtro['concorrentes'].length; i++) {
                    qb = qb.orWhere(`p${i}.fabricante is not null`);
                }
            });
            query.andWhere(qb);
        }

        if (filtro['vantagens']) {
            let qb = new Brackets(qb => {
                qb.where('p0.vantagens like :vantagens');
                for (let i = 1; i < filtro['concorrentes'].length; i++) {
                    qb = qb.orWhere(`p${i}.vantagens like :vantagens`);
                }
            });
            query.andWhere(qb);
            query.setParameter('vantagens', (filtro['vantagens'] as string).toUpperCase());
        }

        if (filtro['vantagensNotNull']) {
            let qb = new Brackets(qb => {
                qb.where('p0.vantagens is not null');
                for (let i = 1; i < filtro['concorrentes'].length; i++) {
                    qb = qb.orWhere(`p${i}.vantagens is not null`);
                }
            });
            query.andWhere(qb);
        }

        if (filtro['precoRegularNull']) {
            let qb = new Brackets(qb => {
                qb.where('p0.precoRegular is null');
                for (let i = 1; i < filtro['concorrentes'].length; i++) {
                    qb = qb.orWhere(`p${i}.precoRegular is null`);
                }
            });
            query.andWhere(qb);
        } else if (filtro['precoRegularIntervalo'][0] || filtro['precoRegularIntervalo'][1]) {
            if (filtro['precoRegularIntervalo'][0] && filtro['precoRegularIntervalo'][1]) {
                let qb = new Brackets(qb => {
                    qb.where('p0.precoRegular between :precoRegularDe and :precoRegularAte');
                    for (let i = 1; i < filtro['concorrentes'].length; i++) {
                        qb = qb.orWhere(`p${i}.precoRegular between :precoRegularDe and :precoRegularAte`);
                    }
                });
                query.andWhere(qb);
                query.setParameter('precoRegularDe', filtro['precoRegularIntervalo'][0]);
                query.setParameter('precoRegularAte', filtro['precoRegularIntervalo'][1]);

            } else if (filtro['precoRegularIntervalo'][0]) {
                let qb = new Brackets(qb => {
                    qb.where('p0.precoRegular >= :precoRegularDe');
                    for (let i = 1; i < filtro['concorrentes'].length; i++) {
                        qb = qb.orWhere(`p${i}.precoRegular >= :precoRegularDe`);
                    }
                });
                query.andWhere(qb);
                query.setParameter('precoRegularDe', filtro['precoRegularIntervalo'][0]);
            } else if (filtro['precoRegularIntervalo'][1]) {
                let qb = new Brackets(qb => {
                    qb.where('p0.precoRegular <= :precoRegularAte');
                    for (let i = 1; i < filtro['concorrentes'].length; i++) {
                        qb = qb.orWhere(`p${i}.precoRegular <= :precoRegularAte`);
                    }
                });
                query.andWhere(qb);
                query.setParameter('precoRegularAte', filtro['precoRegularIntervalo'][1]);
            }
        }

        if (filtro['precoFinalNull']) {
            let qb = new Brackets(qb => {
                qb.where('p0.precoFinal is null');
                for (let i = 1; i < filtro['concorrentes'].length; i++) {
                    qb = qb.orWhere(`p${i}.precoFinal is null`);
                }
            });
            query.andWhere(qb);
        } else if (filtro['precoFinalIntervalo'][0] || filtro['precoFinalIntervalo'][1]) {
            if (filtro['precoFinalIntervalo'][0] && filtro['precoFinalIntervalo'][1]) {
                let qb = new Brackets(qb => {
                    qb.where('p0.precoFinal between :precoFinalDe and :precoFinalAte');
                    for (let i = 1; i < filtro['concorrentes'].length; i++) {
                        qb = qb.orWhere(`p${i}.precoFinal between :precoFinalDe and :precoFinalAte`);
                    }
                });
                query.andWhere(qb);
                query.setParameter('precoFinalDe', filtro['precoFinalIntervalo'][0]);
                query.setParameter('precoFinalAte', filtro['precoFinalIntervalo'][1]);
            } else if (filtro['precoFinalIntervalo'][0]) {
                let qb = new Brackets(qb => {
                    qb.where('p0.precoFinal >= :precoFinalDe');
                    for (let i = 1; i < filtro['concorrentes'].length; i++) {
                        qb = qb.orWhere(`p${i}.precoFinal >= :precoFinalDe`);
                    }
                });
                query.andWhere(qb);
                query.setParameter('precoFinalDe', filtro['precoFinalIntervalo'][0]);
            } else if (filtro['precoFinalIntervalo'][1]) {
                let qb = new Brackets(qb => {
                    qb.where('p0.precoFinal <= :precoFinalAte');
                    for (let i = 1; i < filtro['concorrentes'].length; i++) {
                        qb = qb.orWhere(`p${i}.precoFinal <= :precoFinalAte`);
                    }
                });
                query.andWhere(qb);
                query.setParameter('precoFinalAte', filtro['precoFinalIntervalo'][1]);
            }
        }

        // Obter a quantidade total da pesquisa
        const queryCount = query.clone();
        queryCount.select('COUNT(1) COUNT');
        const count = (await queryCount.getRawOne()).COUNT;

        // Obter a página de registros
        if (filtro['limite']) {
            query.limit(filtro['limite']);
            query.offset(filtro['pagina'] * filtro['limite']);
        }

        query.orderBy('p0.sessao, p0.categoria, p0.subcategoria, p0.nome');

        const result = await query.getRawMany();
        //const count = await query.getCount();
        
        return [result, count];
    }

    async listarEanNaoConciliado(processoBaseId: string, processoDestinoId: string): Promise<string[]> {
        let query = this.manager.createQueryBuilder(Produto, 'p0');

        query = query.select("p0.ean");

        query = query.where("p0.processo.id = :processoBaseId");
        query = query.andWhere("p0.ean NOT IN " + 

            query.subQuery()
                .select('p1.ean')
                .from(Produto, "p1")
                .where("p1.processo.id = :processoDestinoId")
                .getQuery());

        query.setParameter('processoBaseId', processoBaseId);
        query.setParameter(`processoDestinoId`, processoDestinoId);
        
        const list = await query.getRawMany();
        
        return list.map(item => item.p0_EAN);
    }

}