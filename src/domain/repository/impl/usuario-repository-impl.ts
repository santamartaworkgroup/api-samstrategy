import { Service } from "typedi";

import { BaseRepositoryImpl } from "../../../infra/repository/base-repository-impl";

import { Usuario } from "../../entity/usuario";
import { UsuarioRepository } from '../usuario-repository';

@Service('usuarioRepository')
export class UsuarioRepositoryImpl extends BaseRepositoryImpl(Usuario)<Usuario, string> implements UsuarioRepository {

}
