import { BaseRepository } from '../../infra/repository/base-repository';

import { Processo } from './../entity/processo';

export interface ProcessoRepository extends BaseRepository<Processo, string> {
    
}