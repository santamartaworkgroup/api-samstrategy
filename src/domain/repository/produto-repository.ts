import { Concorrente } from '../entity/type/concorrente';
import { BaseRepository } from './../../infra/repository/base-repository';

import { Produto } from './../entity/produto';

export interface ProdutoRepository extends BaseRepository<Produto, string> {
    
    consultarConcorrencia(filtro: any): Promise<[any[], number]>;
    listarEanNaoConciliado(processoBaseId: string, processoDestinoId: string): Promise<string[]>;
    
}