import { BaseRepository } from '../../infra/repository/base-repository';

import { Usuario } from './../entity/usuario';

export interface UsuarioRepository extends BaseRepository<Usuario, string> {
    
}