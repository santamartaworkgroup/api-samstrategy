import { Concorrente } from './../entity/type/concorrente';
import { BaseService } from "../../infra/service/base-service";

import { Analise } from "../entity/analise";
import { Processo } from "../entity/processo";
import { AnaliseRepository } from "../repository/analise-repository";
import { Produto } from '../entity/produto';

export interface AnaliseService extends BaseService<AnaliseRepository, Analise, string>, AnaliseRepository {
    
    conciliar(processoBaseId: string, processoDestinoId: string): Promise<Produto[]>;

}