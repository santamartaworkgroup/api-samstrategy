import { Inject, Service } from 'typedi';
import { Equal } from 'typeorm';
import { ConcorrenteExtractionFactory } from '../../../extracttion/concorrente-extraction';
import { BaseServiceImpl } from '../../../infra/service/base-service-impl';
import { Analise } from "../../entity/analise";
import { Produto } from '../../entity/produto';
import { StatusProcesso } from '../../entity/type/status-processo';
import { AnaliseRepository } from '../../repository/analise-repository';
import { AnaliseService } from '../analise-service';
import { ProdutoService } from '../produto-service';
import { ProcessoService } from './../processo-service';

@Service('analiseService')
export class AnaliseServiceImpl extends BaseServiceImpl<AnaliseRepository, Analise, string> implements AnaliseService {
    constructor(
        @Inject('analiseRepository')
        repository: AnaliseRepository,

        @Inject('processoService')
        private processoServie: ProcessoService,

        @Inject('produtoService')
        private produtoServie: ProdutoService
    ) {
        super( repository );
    }

    async remover(analise: Analise): Promise<Analise> {
        analise = await this.carregar(analise);
        this.checarProcessosEmAndamento(analise);
        await this.processoServie.removerTodos(analise.processos);
        return super.remover(analise);
    }

    async removerTodos(analises: Analise[]): Promise<Analise[]> {
        for(let analise of analises) {
            analise = await this.carregar(analise);
            this.checarProcessosEmAndamento(analise);
            await this.processoServie.removerTodos(analise.processos);
        }
        return super.removerTodos(analises);
    }

    private checarProcessosEmAndamento(analise: Analise) {
        analise.processos.forEach(processo => {
            if (processo.status == StatusProcesso.Processando) {
                throw new Error('Analise com não pode ser excluida com processos de extração em andamento!');
            }
        });
    }

    async conciliar(processoBaseId: string, processoDestinoId: string): Promise<Produto[]> {
        return new Promise<Produto[]>(async (resolve, reject) => {
            let processo = await this.processoServie.obter({ 
                where: { id: Equal(processoDestinoId) },
                relations: ['analise'] 
            });
    
            if (processo === null || processo.status === StatusProcesso.Conciliando) {
                return resolve( [] );
            }

            const EANs = await this.produtoServie.listarEanNaoConciliado(processoBaseId, processoDestinoId);
    
            try {
                processo.status = StatusProcesso.Conciliando;
                processo = await this.processoServie.salvar(processo);
                
                const extract = ConcorrenteExtractionFactory.getConcorrenteExtract(processo.concorrente);
    
                const produtos = await extract.conciliarProdutos(processo, EANs);
        
                processo = await this.processoServie.obter({ id: processo.id });
                if (processo) {
                    processo.status = StatusProcesso.Concluido;
                    await this.processoServie.salvar(processo);
                }
    
                return resolve( produtos );
            } catch (error) {
                processo = await this.processoServie.obter({ id: processo.id });
                if (processo) {
                    processo.status = StatusProcesso.Falhado;
                    processo.erro = error.message;
                    await this.processoServie.salvar(processo);
                }
                return reject(error);
            }
        });
    }

}