import Container from 'typedi';

import { UsuarioServiceImpl } from './usuario-service-impl';
import { AnaliseServiceImpl } from './analise-service-impl';
import { ProcessoServiceImpl } from './processo-service-impl';
import { ProdutoServiceimpl } from './produto-service-impl';

export function registerServices(): void {
    Container.get(UsuarioServiceImpl);
    Container.get(AnaliseServiceImpl);
    Container.get(ProcessoServiceImpl);
    Container.get(ProdutoServiceimpl);
}