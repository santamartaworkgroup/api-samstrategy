import { Service, Inject } from "typedi";

import { BaseServiceImpl } from "../../../infra/service/base-service-impl";

import { StatusProcesso } from './../../entity/type/status-processo';
import { Processo } from "../../entity/processo";
import { ProcessoRepository } from "../../repository/processo-repository";
import { ProcessoService } from './../processo-service';
import { ConcorrenteExtractionFactory } from "../../../extracttion/concorrente-extraction";
import { ProdutoService } from '../produto-service';
import { In } from "typeorm";

@Service('processoService')
export class ProcessoServiceImpl extends BaseServiceImpl<ProcessoRepository, Processo, string> implements ProcessoService {
    private processando = false;
    
    constructor(
        @Inject('processoRepository')
        repository: ProcessoRepository,

        @Inject('produtoService')
        private produtoService: ProdutoService
    ) {
        super( repository );
    }

    async remover(processo: Processo): Promise<Processo> {
        await this.produtoService.deletar({
            processo: { id: processo.id }
        });
        return super.remover(processo);
    }

    async removerTodos(processos: Processo[]): Promise<Processo[]> {
        for(let processo of processos) {
            await this.produtoService.deletar({
                processo: { id: processo.id }
            });
        }
        return super.removerTodos(processos);
    }

    async iniciarProcessos(): Promise<void> {
        setTimeout(
            async () => {
                try {
                    const processos = (await this.repository.listar({ 
                        where: { status: In([ StatusProcesso.Processar, StatusProcesso.Processando ] ) }, 
                        order: { data: 'ASC' }, 
                        relations: ['analise'] 
                    }))[0];
                    for(const processo of processos) {
                        await this.processar(processo)
                    }
                } catch (ignore) {
                } 

                this.iniciarProcessos();
            }, 1000
        );
    }

    async processar(processo: Processo): Promise<Processo> {
        if (this.processando) { return processo; };
        if ([StatusProcesso.Processar, StatusProcesso.Processando].indexOf(processo.status) < 0) { return processo; };

        this.processando = true;
        try {
            await this.produtoService.deletar({ processo: { id: processo.id } });
            
            processo.status = StatusProcesso.Processando;
            processo.inicio = new Date();
            processo.termino = null;
            processo.erro = null;
            processo.totalProcessado = 0;
            processo = await this.salvar(processo);
                
            const extract = ConcorrenteExtractionFactory.getConcorrenteExtract(processo.concorrente);

            processo = await extract.extrairProdutos(processo);

            if (processo) {
                processo.status = StatusProcesso.Concluido;
                processo.termino = new Date();
                processo = await this.salvar(processo);
            }
        } catch (error) {
            if (processo) {
                processo = await this.obter({ id: processo.id });
            }
            if (processo) {
                processo.status = StatusProcesso.Falhado;
                processo.termino = new Date();
                processo.erro = error.message;
                processo = await this.salvar(processo);
            }
        } finally {
            this.processando = false;
        }
        return processo;
    }
    
}