import { Analise } from './../../entity/analise';
import { AnaliseService } from './../analise-service';
import { Service, Inject } from "typedi";

import { BaseServiceImpl } from "../../../infra/service/base-service-impl";

import { Produto } from "../../entity/produto";
import { ProdutoRepository } from "../../repository/produto-repository";
import { ProdutoService } from '../produto-service';
import { ConcorrenteExtractionFactory } from '../../../extracttion/concorrente-extraction';
import { Concorrente } from '../../entity/type/concorrente';

@Service('produtoService')
export class ProdutoServiceimpl extends BaseServiceImpl<ProdutoRepository, Produto, string> implements ProdutoService {
    
    @Inject('analiseService')
    private analiseService: AnaliseService;

    constructor(
        @Inject('produtoRepository')
        repository: ProdutoRepository
    ) {
        super( repository );
    }

    async atualizarProdutos(filtro: any): Promise<void> {
        let analise: Analise = await this.analiseService.obter({id: filtro['analiseId']});
        let concorrencia = await this.consultarConcorrencia(filtro);

        let processosPorConcorrente: any = {};
        let produtosPorConcorrente: any = {};
        for (let i = 0; i < filtro['concorrentes'].length; i++) {
            let concorrente = filtro['concorrentes'][i];
            produtosPorConcorrente[concorrente.nome] = [];
            for (let j = 0; j < analise.processos.length; j++) {
                if (analise.processos[j].concorrente == concorrente.nome) {
                    processosPorConcorrente[concorrente.nome] = analise.processos[j];
                    break;
                }
            }
        }
        concorrencia[0].forEach(p => {
            for (let i = 0; i < filtro['concorrentes'].length; i++) {
                let concorrente = filtro['concorrentes'][i].nome;
                let produtoConcorrente = this.montarProdutoComparacao(p, i);
                produtoConcorrente.analise = analise;
                produtoConcorrente.processo = processosPorConcorrente[concorrente];
                produtoConcorrente.concorrente = concorrente;
                produtosPorConcorrente[concorrente].push(produtoConcorrente);
            }
        });
        for (let i = 0; i < filtro['concorrentes'].length; i++) {
            let concorrente = filtro['concorrentes'][i].nome;
            const extract = ConcorrenteExtractionFactory.getConcorrenteExtract(concorrente);
            produtosPorConcorrente[concorrente] = await extract.atualizarProdutos(produtosPorConcorrente[concorrente]);
        }
    }

    private montarProdutoComparacao(p?: any, i?: number) {
        const produto: Produto = new Produto();
        produto.id         = p[`p${i}_ID`];
        produto.nome         = p[`p${i}_NOME`];
        produto.precoRegular = Math.round(p[`p${i}_PRECO_REGULAR`] * 100) / 100;
        produto.precoFinal   = Math.round(p[`p${i}_PRECO_FINAL`] * 100) / 100;
        produto.sessao    = p[`p${i}_SESSAO`];
        produto.categoria    = p[`p${i}_CATEGORIA`];
        produto.subcategoria = p[`p${i}_SUBCATEGORIA`];
        produto.marca        = p[`p${i}_MARCA`];
        produto.fabricante   = p[`p${i}_FABRICANTE`];
        produto.ean          = p[`p${i}_EAN`];
        produto.sku          = p[`p${i}_SKU`];
        produto.descricao    = p[`p${i}_DESCRICAO`];
        produto.vantagens    = p[`p${i}_VANTAGENS`] ? p[`p${i}_VANTAGENS`] : '';
        produto.peso         = p[`p${i}_PESO`];
        produto.link       = p[`p${i}_LINK`];
        produto.outros       = p[`p${i}_OUTROS`];
        return produto;
    }
    
    consultarConcorrencia(filtro: any): Promise<[any[], number]> {
        return this.repository.consultarConcorrencia(filtro);
    }

    listarEanNaoConciliado(processoBaseId: string, processoDestinoId: string): Promise<string[]> {
        return this.repository.listarEanNaoConciliado(processoBaseId, processoDestinoId);
    }

}