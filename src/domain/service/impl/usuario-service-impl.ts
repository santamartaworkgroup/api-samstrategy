import { Service, Inject } from 'typedi';

import { BaseServiceImpl } from '../../../infra/service/base-service-impl';

import { Usuario } from "../../entity/usuario";
import { UsuarioRepository } from '../../repository/usuario-repository';
import { UsuarioService } from '../usuario-service';

@Service('usuarioService')
export class UsuarioServiceImpl extends BaseServiceImpl<UsuarioRepository, Usuario, string> implements UsuarioService {
    
    constructor(
        @Inject('usuarioRepository')
        repository: UsuarioRepository
    ) {
        super( repository );
    }

    salvar(usuario: Usuario) {
        if (!usuario.nome || usuario.nome.trim().length == 0) {
            throw new Error('Nome obrigatório.');
        }
        if (!usuario.email || usuario.email.trim().length == 0) {
            throw new Error('Email obrigatório.');
        }
        if (!usuario.login || usuario.login.trim().length == 0) {
            throw new Error('Login obrigatório.');
        }
        if (!usuario.senha || usuario.senha.trim().length == 0) {
            throw new Error('Senha obrigatória.');
        }
        return super.salvar(usuario);
    }

}