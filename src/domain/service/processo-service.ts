import { BaseService } from "../../infra/service/base-service";

import { Processo } from "../entity/processo";
import { ProcessoRepository } from "../repository/processo-repository";

export interface ProcessoService extends BaseService<ProcessoRepository, Processo, string>, ProcessoRepository {
    
    processar(processo: Processo): Promise<Processo>;
    iniciarProcessos() : Promise<void>;
    
}