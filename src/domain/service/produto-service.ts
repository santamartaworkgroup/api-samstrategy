import { BaseService } from "../../infra/service/base-service";

import { Produto } from "../entity/produto";
import { ProdutoRepository } from "../repository/produto-repository";

export interface ProdutoService extends BaseService<ProdutoRepository, Produto, string>, ProdutoRepository {
    
    consultarConcorrencia(filtro: any): Promise<[any[], number]>;
    atualizarProdutos(filtro: any): Promise<void>;

}