import { BaseService } from "../../infra/service/base-service";

import { Usuario } from "../entity/usuario";
import { UsuarioRepository } from "../repository/usuario-repository";

export interface UsuarioService extends BaseService<UsuarioRepository, Usuario, string>, UsuarioRepository {
    
}