import cheerio from 'cheerio';
import puppeteer, { Browser, Page } from 'puppeteer';
import request from 'request';
import { Inject, Service } from 'typedi';
import { Analise } from '../domain/entity/analise';
import { Processo } from '../domain/entity/processo';
import { ProcessoService } from '../domain/service/processo-service';
import { ProdutoService } from '../domain/service/produto-service';
import { Produto } from './../domain/entity/produto';
import { Concorrente } from './../domain/entity/type/concorrente';
import { ConcorrenteExtracttion } from './concorrente-extraction';

const WAIT_FOR_SELECTOR = 5000;
const LAUNCH_OPTIONS = { headless: true, devtools: false, args: ['--no-sandbox', '--disable-setuid-sandbox'] };

@Service()
export class AraujoExtraction implements ConcorrenteExtracttion {

  private static SITE = 'https://www.araujo.com.br';

  @Inject('produtoService')
  produtoService: ProdutoService;

  @Inject('processoService')
  processoService: ProcessoService;

  private processo: Processo;
  private analise: Analise;
  private eans: string[];

  private browser: Browser;

  extrairProdutos(processo: Processo): Promise<Processo> {
    return new Promise(async (resolve, reject) => {
      this.eans = [];
      this.processo = processo;
      this.analise = processo.analise;
      try { 
        let links = await this.listarLinksCategoriaSubcategoria(AraujoExtraction.SITE);
        links = links.filter(link => link != '/medicamentos-especiais');
        
        for(let link of links) {
          await this.processarCategoriaSubcategoria(link);
          if (!this.processo) { break; }
        }
        this.eans = [];

        if (this.browser) this.browser.close();
        
        return resolve(this.processo);
      } catch (error) {
        if (this.browser) this.browser.close();
        return reject(error);
      }
    });
  }

  private listarLinksCategoriaSubcategoria(link: string): Promise<string[]> {
    return new Promise<string[]>(async (resolve, reject) => {
      try {
        let $ = await this.load(link);
        let links: any = [];
        let linksProdutos = await this.listarLinks($, `.header-amazing-menu > ul > li > a`)
        for (let i = 1; i < linksProdutos.length - 1; i++) {
          links.push(linksProdutos[i]);
        }
        return resolve(links);
      } catch (error) {
        return reject(error);
      }
    });
  }

  private processarCategoriaSubcategoria(link: string): Promise<void> {
    return new Promise<void>(async (resolve, reject) => {
      if (global.gc) global.gc();
      
      let produtos: Produto[] = [];

      let attemptCount = 0;
      this.browser = this.browser ? this.browser : await puppeteer.launch( LAUNCH_OPTIONS );
      const page = await this.newPage(this.browser, `${AraujoExtraction.SITE + link}?PS=50`);
      let pageNumber = 1;
      while (true) {
        try {
          const linksProdutos = await this.listarLinksProdutosPuppeteer(page, pageNumber++, '.qd_cpProductLink.qd-cpProductName');
          produtos = produtos.concat(await this.listarProdutos(linksProdutos));
          if (linksProdutos.length == 0 || (await page.$$('.next.pgEmpty')).length > 0) {
            page.close();
            break;
          }
          page.click('.next');
        } catch (error) {
          if (attemptCount++ >= 10) return reject(error)
          await this.sleep(60000);
        }
      } 

      try {
        this.processo = await this.processoService.obter({ id: this.processo.id });
        if (this.processo && produtos.length > 0) {
          produtos = await this.produtoService.salvarTodos(produtos);
          this.processo.totalProcessado += produtos.length; // Parcial
          await this.processoService.salvar(this.processo);
        } 
        resolve();
      } catch (error) {
        return reject(error);
      }
    });
  }

  private listarProdutos(linksProdutos: string[]): Promise<Produto[]> {
    return new Promise<Produto[]>(async (resolve, reject) => {
      try {
        const produtos: Produto[] = [];
        while (produtos.length < linksProdutos.length) {
          if (global.gc) global.gc();

          const produtoPromisses: Promise<Produto>[] = [];
          for(let i = produtos.length; i < produtos.length + linksProdutos.length; i++) {
            if (i == linksProdutos.length) break;
            produtoPromisses.push(this.extrairProduto(linksProdutos[i]).catch( err => { throw err }));
          }
  
          produtos.push( ...await Promise.all(produtoPromisses) ) ;
        }

        for(const produto of produtos) {
          produto.analise = this.analise;
          produto.processo = this.processo;
          produto.concorrente = Concorrente.Araujo;
        }

        return resolve(produtos.filter(produto => produto.ean));
      } catch (error) {
        return reject(error);
      }
    });
  }

  private extrairProduto(link: string): Promise<Produto> {
    return new Promise<Produto>(async (resolve, reject) => {
      const produto = new Produto();

      try {
        let $ = await this.load(link);

        const produto = new Produto;
        
        let descricao = $('.value-field .Indicacao');
        produto.descricao = descricao ? descricao.text() : null;
        if (produto.descricao && produto.descricao.length > 4000) produto.descricao = produto.descricao.substring(0, 4000);
        
        let produtoJson:any = null;
        let totalScripts = $('script').length;
        for (let i = 0; i < totalScripts; i++) {
          produtoJson = $('script')[i];
          if (produtoJson.firstChild && produtoJson.firstChild.data.indexOf('vtex.events.addData') != -1) {
            break;
          }
        }
        
        produtoJson = produtoJson.firstChild.data;
        produtoJson = produtoJson.substring(produtoJson.indexOf('{'), produtoJson.indexOf(');'));
        produtoJson = JSON.parse(produtoJson);

        produto.ean = produtoJson.productEans[0];
        if (this.eans && this.eans.filter(prod => prod === produto.ean).length > 0) {
          produto.ean = undefined;
          return resolve(produto);
        }
        if (this.eans) this.eans.push(produto.ean);

        let breadcrumb = $('.bread-crumb > ul > li > a > span');
        let sessao = breadcrumb && breadcrumb.length > 1 ? breadcrumb[1].firstChild.data : undefined;
        let categoria = produtoJson.productCategoryName;

        produto.sku = produtoJson.productId;
        produto.nome = produtoJson.productName ? produtoJson.productName.toUpperCase() : '';
        produto.precoRegular = produtoJson.productListPriceFrom;
        produto.precoFinal = produtoJson.productPriceTo;
        produto.marca = produtoJson.productBrandName.toUpperCase();
        produto.link = link;
        produto.sessao = sessao;
        produto.categoria = categoria;
        produto.ultimaAtualizacao = new Date();

        let vantagensDiv = $('.qd-highlight-product-condition');
        produto.vantagens = vantagensDiv ? vantagensDiv.text() : null;
        if (produto.vantagens) {
          while (produto.vantagens.indexOf("  ") >= 0) {
            produto.vantagens = produto.vantagens.replace("  ", " ");
          }
          produto.vantagens = produto.vantagens.trim().toUpperCase();
        }
        
        /*$ = await this.load(`https://drogariaspacheco.vteximg.com.br/produto/sku/${produto.sku}`)*/

        return resolve(produto);
      } catch (error) {
        return resolve(produto);
      }
    });
  }

  private listarLinks($: CheerioStatic, selector: string): Promise<string[]> {
    return new Promise<string[]>(async (resolve, reject) => {
      try {
        const links: string[] = []
        $(selector).each( (i, e) => {
          links.push(e.attribs['href']);
        });
        return resolve(links);
      } catch (error) {
        return reject(error);
      }
    });
  }

  private listarLinksProdutosPuppeteer(page: Page, pageNumber: number, selector: string): Promise<string[]> {
    return new Promise<string[]>(async (resolve, reject) => {
      
      try {
        await page.waitForSelector('.page-number.pgCurrent', { timeout: WAIT_FOR_SELECTOR });
      } catch (error) {
        return reject(error);
      }

      let currentPageNumber = '';
      do {
        const currentPageList = await page.$$('.page-number.pgCurrent');
        currentPageNumber = await currentPageList[0].evaluate( pn => pn.textContent) as string;
      } while (pageNumber.toString() != currentPageNumber);

      try {
        const anchorHandleList = await page.$$(selector);
      
        const links = []
        for(const anchorHandle of anchorHandleList) {
          const link = await anchorHandle.evaluate( anchor => anchor.getAttribute("href")) as string;
          links.push(link);
        }

        return resolve(links);
      } catch (error) {
        return reject(error);
      }
    });
  }

  private load(link: string): Promise<CheerioStatic> {
    return new Promise<CheerioStatic>(async (resolve, reject) => {
      try {
        request(link, (err, res, body) => {
          if (err) return reject(new Error(err));
          return resolve (cheerio.load(body));           
        })
      } catch (error) {
        return reject(error);
      }
    });
  }

  private sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  private newPage(browser: Browser, link: string): Promise<Page> {
    return new Promise<Page>(async (resolve, reject) => {
      try {
        const page = await browser.newPage();
        await page.goto(link, { timeout: 300000 });
        return resolve(page);
      } catch (error) {
        return reject(error);
      }
    });
  }
  
  async atualizarProdutos(produtos: Produto []): Promise<Produto []> {
    let produtosAtualizados = [];
    for(let produto of produtos) {
      let produtoTemp = await this.extrairProduto(produto.link);
      produtoTemp.id = produto.id;
      produtoTemp.analise = produto.analise;
      produtoTemp.processo = produto.processo;
      produtoTemp.concorrente = produto.concorrente;
      produtoTemp.sessao = produto.sessao;
      produtoTemp.categoria = produto.categoria;
      produtoTemp.subcategoria = produto.subcategoria;
      produtosAtualizados.push(produtoTemp);
    }
    return await this.produtoService.salvarTodos(produtosAtualizados);
  }

  async conciliarProdutos(processo: Processo, EANs: string[]): Promise<Produto[]> {
    return new Promise(async (resolve, reject) => {
      try {
              
        // TODO

        return reject(new Error(`Processo de conciliar produtos não implementado!`));
      } catch (error) {
        return reject(error);
      }
    });
  }
}
