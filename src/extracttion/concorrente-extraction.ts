import { Container } from "typedi";
import { useContainer } from "typeorm";
import { Concorrente } from '../domain/entity/type/concorrente';
import { Processo } from './../domain/entity/processo';
import { Produto } from './../domain/entity/produto';
import { AraujoExtraction } from "./araujo-extraction";
import { DrogasilExtraction } from './drogasil-extraction';
import { PachecoExtraction } from "./pacheco-extraction";
import { PagueMenosExtraction } from "./pague-menos-extraction";
import { PanvelExtraction } from './panvel-extraction';
import { SantaMartaExtraction } from "./santa-marta-extraction";

useContainer(Container);

export interface ConcorrenteExtracttion {
    extrairProdutos(processo: Processo): Promise<Processo>;
    atualizarProdutos(produtos: Produto []): Promise<Produto[]>;
    conciliarProdutos(processo: Processo, EANs: string[]): Promise<Produto[]>;
}

export abstract class ConcorrenteExtractionFactory {
    static getConcorrenteExtract(concorrente: Concorrente): ConcorrenteExtracttion {
        switch (concorrente) {
            case Concorrente.SantaMarta:
                return Container.get(SantaMartaExtraction);

            case Concorrente.Drogasil:
                return Container.get(DrogasilExtraction);

            case Concorrente.Pacheco:
                return Container.get(PachecoExtraction);

            case Concorrente.PagueMenos:
                return Container.get(PagueMenosExtraction);

            case Concorrente.Araujo:
                return Container.get(AraujoExtraction);

            case Concorrente.Panvel:
                return Container.get(PanvelExtraction);
            
            default:
                throw new Error('Extração de Concorrente não implementado');
        };
    }
}