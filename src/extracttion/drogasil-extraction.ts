import cheerio from 'cheerio';
import { configure, getLogger } from "log4js";
import puppeteer, { Browser, Page } from 'puppeteer';
import superagent from 'superagent';
import { Inject, Service } from 'typedi';
import { Analise } from '../domain/entity/analise';
import { Processo } from '../domain/entity/processo';
import { Produto } from '../domain/entity/produto';
import { Concorrente } from '../domain/entity/type/concorrente';
import { ProcessoService } from '../domain/service/processo-service';
import { ProdutoService } from '../domain/service/produto-service';
import { ConcorrenteExtracttion } from './concorrente-extraction';

const LAUNCH_OPTIONS = { headless: true, devtools: false, args: ['--no-sandbox'] };
const WAIT_FOR_SELECTOR = 10000;

configure("./log4js-config.json");
const logger = getLogger();
logger.level = "debug";

@Service()
export class DrogasilExtraction implements ConcorrenteExtracttion {
  private static SITE = 'https://www.drogasil.com.br';

  @Inject('produtoService')
  produtoService: ProdutoService;

  @Inject('processoService')
  processoService: ProcessoService;

  private processo: Processo;
  private analise: Analise;
  private eans: any [];

  extrairProdutos(processo: Processo): Promise<Processo> {
    return new Promise(async (resolve, reject) => {
      logger.debug("Iniciando a extração de produtos da Drogasil");

      this.processo = processo;
      this.analise = processo.analise;
      this.eans = [];
      
      try {
        const links = await this.listarLinksSessaoCategoriaSubcategoria(`${DrogasilExtraction.SITE}`);
        logger.debug(`Total de links listados ${links.length}`);
        
        for(let link of links) {
          logger.debug(`Processando o link ${link}`);

          let pageCount = 1;
          let linkPromisses: Promise<boolean>[] = [];
          while (true) {
            linkPromisses.push( this.processarSessaoCategoriaSubcategoria(`${link}?p=${pageCount}`) );

            const retruns: boolean[] = [];
            if (pageCount % 2 == 0) {
              retruns.push( ...await Promise.all(linkPromisses) ) ;
              linkPromisses = [];
            }

            if (!retruns.reduce((resp, ret) => resp && ret, true)) { break; }
            
            if (!this.processo) { break; }

            pageCount++;
          }

          if (!this.processo) { break; }
        }

        this.eans = [];
        return resolve(this.processo);
      } catch (error) {
        logger.error(`extrairProdutos: ${error}`);
        return reject(error);
      }
    });
  }

  private listarLinksSessaoCategoriaSubcategoria(link: string): Promise<string[]> {
    return new Promise<string[]>(async (resolve, reject): Promise<void> => {
      logger.debug("Listando os links de sessão/categoria/subcategoria da Drogasil");

      try {
        let $ = await this.load(link);

        const links: string[] = []

        //let sessaoLinks = await this.listarLinks($, "li > a[role='link'][data-qa='list_menu']");
        let allLinks = await this.listarLinks($, "a[role='link']");
        
        for (let count = 0; count < allLinks.length; count++) {
          const sessaoLink = allLinks[count];
          const sessao = sessaoLink.substring(0, sessaoLink.lastIndexOf('.'));

          let categoria: string;
          let categoriaLink: string;
          let categoriaCount = 0;
          do {
            if (++count >= allLinks.length) break;
            
            categoriaLink = allLinks[count];
            categoria = categoriaLink.substring(0, categoriaLink.lastIndexOf('.'));

            if (categoriaLink.startsWith(sessao)) {
              categoriaCount++;

              let subcategoriaLink: string;
              let subcategoriaCount = 0;
              do {
                if (++count >= allLinks.length) break;
      
                subcategoriaLink = allLinks[count];
                if (subcategoriaLink.startsWith(categoria)) {
                  subcategoriaCount++;
                  links.push(subcategoriaLink);
                }
              } while (subcategoriaLink.startsWith(categoria));

              if (subcategoriaCount == 0) {
                links.push(categoriaLink);
              }

              if (count >= allLinks.length) break;
              count--;
            }

          } while (categoriaLink.startsWith(sessao));

          if (categoriaCount == 0) {
            links.push(sessaoLink);
          }

          if (count >= allLinks.length) break;
          count--;
        }

        return resolve(links.filter(link => 
          //!link.startsWith('/drogasil-marcas-exclusivas') && 
          !link.startsWith('/promocoes-do-mes') &&
          !link.endsWith('/todos-de-a-z.html') &&
          !link.endsWith('/genericos-de-a-z.html')
        ));
      } catch (error) {
        logger.error(`listarLinksSessaoCategoriaSubcategoria: ${error}`);
        return reject(error);
      }
    });
  }

  private processarSessaoCategoriaSubcategoria(link: string): Promise<boolean> {
    return new Promise<boolean>(async (resolve, reject) => {
      if (!link) return resolve(false);

      if (global.gc) global.gc(); // NODE_OPTIONS=--expose-gc

      let path = link.substring(1, link.lastIndexOf("."));

      const sessao = path.substring(0, (path.indexOf("/") >= 0 ? path.indexOf("/") : path.length));
      path = path.indexOf("/") >= 0 ? path.substring(path.indexOf("/") + 1, path.length) : '';

      const categoria = path.substring(0, (path.indexOf("/") >= 0 ? path.indexOf("/") : path.length));
      path = path.indexOf("/") >= 0 ? path.substring(path.indexOf("/") + 1, path.length) : '';

      const subcategoria = path.substring(0, (path.indexOf("/") >= 0 ? path.indexOf("/") : path.length));
      path = path.indexOf("/") >= 0 ? path.substring(path.indexOf("/") + 1, path.length) : '';

      try {
        logger.debug(`Lendo a pagina ${DrogasilExtraction.SITE}/${link}`);

        let $: CheerioStatic;
        try {
          $ = await this.load( `${DrogasilExtraction.SITE}/${link}` );
        } catch (error) {
          return resolve(false);
        }

        const linksProdutos = await this.listarLinksProdutos($);
        let produtos: Produto[] = await this.listarProdutos(linksProdutos, sessao, categoria, subcategoria);

        if (produtos.length) {
          this.processo = await this.processoService.obter({ id: this.processo.id });
          if (this.processo) {

            produtos = await this.produtoService.salvarTodos(produtos);
      
            this.processo.totalProcessado += produtos.length; // Parcial
            this.processo = await this.processoService.salvar(this.processo);
          } else {
            return resolve(null);
          }
        }

        if (link.indexOf('?') > 0) {
          link = link.substring(0, link.indexOf('?'));
        }
        
        return resolve(produtos.length > 0);
      } catch (error) {
        logger.error(`processarSessaoCategoriaSubcategoria: ${error}`);
        return reject(error);
      }
    });
  }

  private listarProdutos(linksProdutos: string[], sessao:string, categoria: string, subcategoria: string): Promise<Produto[]> {
    return new Promise<Produto[]>(async (resolve, reject) => {
      try {
        logger.debug(`Extraindo os produtos da pagina: ${linksProdutos.length} produtos`);

        const produtos: Produto[] = [];
        while (produtos.length < linksProdutos.length) {
          if (global.gc) global.gc(); // NODE_OPTIONS=--expose-gc

          const produtoPromisses: Promise<Produto>[] = [];
          for(let i = produtos.length; i < produtos.length + linksProdutos.length; i++) {
            if (i == linksProdutos.length) break;
            produtoPromisses.push(this.extrairProduto(linksProdutos[i], sessao, categoria, subcategoria).catch( err => { throw err }));
          }
  
          produtos.push( ...await Promise.all(produtoPromisses) ) ;
        }

        return resolve(produtos.filter(produto => produto && produto.ean).map(produto => {
          produto.analise = this.analise;
          produto.processo = this.processo;
          produto.concorrente = Concorrente.Drogasil;
          return produto;
        }));
      } catch (error) {
        logger.error(`listarProdutos: ${error}`);
        return reject(error);
      }
    });
  }
  
  private extrairProduto(link: string, sessao:string, categoria: string, subcategoria: string): Promise<Produto> {
    return new Promise<Produto>(async (resolve, reject) => {
      try {
        let $ = await this.load(link);
        if ($('.alert-stock').length > 0) {
          return resolve(null);
        }

        const produto = new Produto();

        /*
        let tableTitles: string[] = [];
        let tableValues: string[] = [];

        await Promise.all([
          this.obterTableTitles($).then( r => tableTitles = r ).catch( err => { throw err } ),
          this.obterTableValues($).then( r => tableValues = r ).catch( err => { throw err } )
        ]);

        produto.sku = tableValues.length > 0 ? tableValues[0] : '';
        produto.ean = tableValues.length > 1 ? tableValues[1] : '';
        produto.sessao = sessao.toUpperCase();
        produto.categoria = categoria.toUpperCase();
        produto.subcategoria = subcategoria.toUpperCase();

        if (tableTitles[1] !== 'EAN' || (this.eans && this.eans.filter((prod: any) => prod === produto.ean + produto.sessao + produto.categoria + produto.subcategoria).length > 0)) {
          produto.ean = undefined;
          return resolve(produto);
        }
        if (this.eans) this.eans.push(produto.ean + produto.sessao + produto.categoria + produto.subcategoria);

        produto.peso = tableValues.length > 2 ? tableValues[2].trim() : '';
        produto.quantidade = tableValues.length > 3 ? tableValues[3].trim() : '';
        produto.marca = tableValues.length > 4 ? tableValues[4].trim().toUpperCase() : '';
        produto.fabricante = tableValues.length > 5 ? tableValues[5].trim().toUpperCase() : '';
        produto.ultimaAtualizacao = new Date();
        produto.link = link;

        await Promise.all([
          this.obterNome($).then( r => produto.nome = r ).catch( err => { throw err } ),
          this.obterDescricao($).then( r => produto.descricao = r ).catch( err => { throw err } ),

          //this.obterPrecoFinal($).then( r => produto.precoFinal = r ).catch( err => { throw err } ),
          //this.obterPrecoRegular($).then( r => produto.precoRegular = r ).catch( err => { throw err } ),
          //this.obterVantagens($).then( r => produto.vantagens = r ).catch( err => { throw err } ),

          this.obterTableTitles($).then( r => tableTitles = r ).catch( err => { throw err } ),
          this.obterTableValues($).then( r => tableValues = r ).catch( err => { throw err } )
        ]);

        const obj = JSON.parse($('#__NEXT_DATA__')[0].firstChild.data);
        const productBySku = obj.props.pageProps.pageData.productData.productBySku;
        produto.precoFinal = productBySku.price_aux.value_to;
        produto.precoRegular = productBySku.price_aux.value_from;
        if (productBySku.price_aux.lmpm_value_to) {
          produto.vantagens = `LEVE ${productBySku.price_aux.lmpm_qty} UNIDADES POR R$ ${productBySku.price_aux.lmpm_value_to} CADA`;
        }

        produto.outros = '';
        for(let i = 6; i < tableTitles.length; i++) {
          produto.outros += `${tableTitles[i]}: ${tableValues[i]}, `;
        }
        if (produto.outros) {
          produto.outros = produto.outros.substring(0, produto.outros.length -2);
          produto.outros = produto.outros.substring(0, 4000);
        }
        */
        const obj = JSON.parse($('#__NEXT_DATA__')[0].firstChild.data);
        const productBySku = obj.props.pageProps.pageData.productData.productBySku;

        produto.concorrente = Concorrente.Drogasil;
        produto.sessao = sessao.toUpperCase();
        produto.categoria = categoria.toUpperCase();
        produto.subcategoria = subcategoria.toUpperCase();

        produto.sku = productBySku.sku;
        produto.nome = productBySku.name.toUpperCase().substring(0, 250);

        produto.precoFinal = productBySku.price_aux.value_to;
        produto.precoRegular = productBySku.price_aux.value_from;
        if (productBySku.price_aux.lmpm_value_to) {
          produto.vantagens = `LEVE ${productBySku.price_aux.lmpm_qty} UNIDADES POR R$ ${productBySku.price_aux.lmpm_value_to} CADA`;
        }

        produto.ean = this.getCustomAttribute('ean', productBySku);

        produto.descricao = this.getCustomAttribute('meta_description', productBySku).substring(0, 4000);
        produto.peso = this.getCustomAttribute('pesokg', productBySku).substring(0, 250);
        produto.quantidade = this.getCustomAttribute('quantidade', productBySku).substring(0, 250);
        produto.marca = this.getCustomAttribute('marca', productBySku).toUpperCase().substring(0, 250);
        produto.fabricante = this.getCustomAttribute('fabricante', productBySku).toUpperCase().substring(0, 250);
        produto.ultimaAtualizacao = new Date();
        produto.link = link;

        logger.debug(`Extraindo oproduto: ${produto.nome} ean ${produto.ean} `);

        return resolve(produto);
      } catch (error) {
        return resolve(null);
      }
    });
  }

  private getCustomAttribute(field: string, productBySku: any): string {
      for(let attr of productBySku.custom_attributes) {
        if (attr.attribute_code == field) {
          if (!attr.value && attr.value_string && attr.value_string.length) {
            return attr.value_string[0].trim();
          }
          if (attr.value && attr.value.length) {
            return attr.value[0].label.trim();
          }
        }
      }
      return '';
  }

  private obterNome($: CheerioStatic): Promise<string> {
    return new Promise<string>(async (resolve, reject) => {
      try {
        const nomeSpan = $('div.column1 > div > h1');
        const nome = nomeSpan ? nomeSpan.text().trim().toUpperCase() : null;
        return resolve(nome);
      } catch (error) {
        return reject(error);
      }
    });
  }

  private obterDescricao($: CheerioStatic): Promise<string> {
    return new Promise<string>(async (resolve, reject) => {
      try {
        const descricaoDiv = $('div#ancorDescription > div.rd-row > div > div > div');
        const descricao = descricaoDiv ? descricaoDiv.text().substring(0, 4000) : null;
        return resolve(descricao);
      } catch (error) {
        return reject(error);
      }
    });
  }  

  private obterPrecoRegular($: CheerioStatic): Promise<number> {
    return new Promise<number>(async (resolve, reject) => {
      try {
        const obj = JSON.parse($('#__NEXT_DATA__')[0].firstChild.data);
        const productBySku = obj.props.pageProps.pageData.productBySku;
        const precoRegular = productBySku.price_aux.value_from;
        return resolve(precoRegular);
      } catch (error) {
        return reject(error);
      }
    });
  }    

  private obterPrecoFinal($: CheerioStatic): Promise<number> {
    return new Promise<number>(async (resolve, reject) => {
      try {
        const obj = JSON.parse($('#__NEXT_DATA__')[0].firstChild.data);
        const productBySku = obj.props.pageProps.pageData.productBySku;
        const precoFinal = productBySku.price_aux.value_to;
        return resolve(precoFinal);
      } catch (error) {
        return reject(error);
      }
    });
  }

  private obterVantagens($: CheerioStatic): Promise<string> {
    return new Promise<string>(async (resolve, reject) => {
      try {
        let vantagens = null;
        const obj = JSON.parse($('#__NEXT_DATA__')[0].firstChild.data);
        const productBySku = obj.props.pageProps.pageData.productBySku;
        if (productBySku.price_aux.lmpm_value_to) {
          vantagens = `LEVE ${productBySku.price_aux.lmpm_qty} UNIDADES POR R$ ${productBySku.price_aux.lmpm_value_to} CADA`;
        }
        return resolve(vantagens);
      } catch (error) {
        return reject(error);
      }
    });
  }    

  private obterTableTitles($: CheerioStatic): Promise<string[]> {
    return new Promise<string[]>(async (resolve, reject) => {
      try {
        const tableTitles: string[] = [];
        $('table>tbody>tr>th').each( (i, e) => {
          tableTitles.push($(e).text());
        });
        return resolve(tableTitles);
      } catch (error) {
        return reject(error);
      }
    });
  }      

  private obterTableValues($: CheerioStatic): Promise<string[]> {
    return new Promise<string[]>(async (resolve, reject) => {
      try {
        const tableValues: string[] = [];
        $('table>tbody>tr>td').each( (i, e) => {
          tableValues.push($(e).text());
        });
        return resolve(tableValues);
      } catch (error) {
        return reject(error);
      }
    });
  }      

  private listarLinks($: CheerioStatic, selector: string): Promise<string[]> {
    return new Promise<string[]>(async (resolve, reject) => {
      try {
        const links: string[] = []
        $(selector).each( (i, e) => {
          links.push(e.attribs['href']);
        });
        return resolve(links);
      } catch (error) {
        return reject(error);
      }
    });
  }

  private listarLinksProdutos($: CheerioStatic): Promise<string[]> {
    return new Promise<any[]>(async (resolve, reject) => {
      try {
        if (global.gc) global.gc(); // NODE_OPTIONS=--expose-gc

        const links: string[] = [];

        const obj = JSON.parse($('#__NEXT_DATA__')[0].firstChild.data);

        for(let item of obj.props.pageProps.pageData.items) {
          let link = this.getCustomAttribute('url_key', item);
          link = `${DrogasilExtraction.SITE}/${link}.html`;
          links.push(link);
        }
        logger.debug(`Total de links de produto  ${links.length}`);

        return resolve(links);
      } catch (error) {
        return reject(error);
      }
    });
  }

  private load(link: string, retry: boolean = true): Promise<CheerioStatic> {
    return new Promise<CheerioStatic>(async (resolve, reject) => {

      let attemptCount = 0;
      while (true) {
        try {
          //const body = await request(link);
          //const {data} = await axios.get(link);
          //const {body} = await got(link);

          const {text} = await superagent.get(link);
          return resolve (cheerio.load(text));
        } catch (error) {
          if (!retry) {
            return reject(error);
          }
          if (attemptCount++ >= 2) return reject(error);
          await this.sleep(1000);
        }
      } 
    });
  }

  private sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  private stringToCurrency(value: string): number {
    if (value) {
      value = value.replace("R$", "");
      value = value.replace(".", "");
      value = value.replace(",", ".");
      value = value.trim();
      return isNaN(Number(value)) ? null : Number(value);
    }
    return null;
  }

  async atualizarProdutos(produtos: Produto []): Promise<Produto []> {
    let produtosAtualizados = [];
    for(let produto of produtos) {
      let produtoTemp = await this.extrairProduto(produto.link, produto.sessao, produto.categoria, produto.subcategoria);
      produtoTemp.id = produto.id;
      produtoTemp.analise = produto.analise;
      produtoTemp.processo = produto.processo;
      produtoTemp.concorrente = produto.concorrente;
      produtoTemp.sessao = produto.sessao;
      produtoTemp.categoria = produto.categoria;
      produtoTemp.subcategoria = produto.subcategoria;
      produtosAtualizados.push(produtoTemp);
    }
    return await this.produtoService.salvarTodos(produtosAtualizados);
  }

  async conciliarProdutos(processo: Processo, EANs: string[]): Promise<Produto[]> {
    return new Promise<Produto[]>(async (resolve, reject) => {
      logger.debug("Pesquisando os EAN da base na pagina do concorrente");

      this.processo = processo;
      this.analise = processo.analise;

      // Obter a lista d links de produtos a conciliar
      let browser: Browser = null;
      try {
        browser = await puppeteer.launch( LAUNCH_OPTIONS );
        
        let linksProdutos: string[] = [];
        
        let eanCount = 0;
        while (eanCount < EANs.length) {
          if (global.gc) global.gc(); // NODE_OPTIONS=--expose-gc

          const linkPromisses: Promise<string[]>[] = [];
          for(let searchCount = 0; searchCount < 100; searchCount++) {

            let searchEans = '';
            for(let i = 0; i < 24; i++) {
              if (eanCount == EANs.length) break;
              searchEans += `${EANs[eanCount]}+`
              eanCount++;
            }

            if (searchEans.length) {
              searchEans = searchEans.substring(0, searchEans.length -1);

              let link = `${DrogasilExtraction.SITE}/search?w=${searchEans}`;
  
              const page = await this.newPagePuppeteer(browser, link);
  
              linkPromisses.push(
                this.listarLinksPuppeteer(
                  page, 
                  'div.product-card-name > a')
                  .then( retorno => { 
                    page.close(); 
                    return retorno; 
                  })
                );
            }
          }

          if (linkPromisses.length) {
            const listLinks = await Promise.all(linkPromisses);
    
            listLinks.forEach(links => linksProdutos.push(...links));
          }
        }

        linksProdutos = linksProdutos.filter(link => link);

        // Extrair do site e gravar os produtos 
        let produtoCount = 0;
        const produtos: Produto[] = [];
        while (produtoCount < linksProdutos.length) {
          if (global.gc) global.gc(); // NODE_OPTIONS=--expose-gc

          const produtoPromisses: Promise<Produto>[] = [];
          for(let i = 0; i < 100; i++) {
            if (produtoCount == linksProdutos.length) break;
            produtoCount++;
            produtoPromisses.push(this.extrairProduto(linksProdutos[produtoCount], '', '', ''));
          }

          if (produtoPromisses.length) {
            const lista: Produto[] = [...await Promise.all(produtoPromisses)]
              .filter(produto => produto && produto.ean).map(produto => {
                produto.analise = this.analise;
                produto.processo = this.processo;
                produto.concorrente = Concorrente.Drogasil;
                return produto;
              }
            )
  
            this.processo = await this.processoService.obter({ id: this.processo.id });
            if (this.processo) {

              const produtosSalvos = await this.produtoService.salvarTodos( lista )

              produtos.push( ...produtosSalvos ) ;

              this.processo.totalProcessado += produtosSalvos.length; // Parcial
              this.processo = await this.processoService.salvar(this.processo);
            } else {
              return resolve(null);
            }
          }
        }

        resolve(produtos);
      } catch (error) {
        return reject(error);
      } finally {
        if (browser) await browser.close();
      }
    });
  }

  private newPagePuppeteer(browser: Browser, link: string): Promise<Page> {
    return new Promise<Page>(async (resolve, reject) => {
      try {
        const page = await browser.newPage();
        await page.goto(link, { timeout: 300000 });
        return resolve(page);
      } catch (error) {
        return reject(error);
      }
    });
  }

  private listarLinksPuppeteer(page: Page, selector: string): Promise<string[]> {
    return new Promise<string[]>(async (resolve, reject) => {
      const links = [];
      try {
        await page.waitForSelector(selector, { timeout: WAIT_FOR_SELECTOR });
      
        const anchorHandleList = await page.$$(selector);
      
        for(const anchorHandle of anchorHandleList) {
          const link = await anchorHandle.evaluate( anchor => anchor.getAttribute("href")) as string;
          links.push(link);
        }

        return resolve(links);
      } catch (error) {
        return resolve(links);
      }
    });
  }

}
