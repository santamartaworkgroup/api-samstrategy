import cheerio from 'cheerio';
import request from 'request';
import { Inject, Service } from 'typedi';
import { Processo } from '../domain/entity/processo';
import { ProcessoService } from '../domain/service/processo-service';
import { ProdutoService } from '../domain/service/produto-service';
import { Analise } from './../domain/entity/analise';
import { Produto } from './../domain/entity/produto';
import { Concorrente } from './../domain/entity/type/concorrente';
import { ConcorrenteExtracttion } from './concorrente-extraction';




@Service()
export class PachecoExtraction implements ConcorrenteExtracttion {
  private static SITE = 'https://www.drogariaspacheco.com.br/';

  @Inject('produtoService')
  produtoService: ProdutoService;

  @Inject('processoService')
  processoService: ProcessoService;

  private processo: Processo;
  private analise: Analise;
  private eans: any [];

  extrairProdutos(processo: Processo): Promise<Processo> {
    return new Promise(async (resolve, reject) => {
      this.processo = processo;
      this.analise = processo.analise;
      this.eans = [];
      try { 
        const links = await this.listarLinksCategoriaSubcategoria(PachecoExtraction.SITE);
        
        for(let link of links) {
          let pageNumber = 1;
          while (link) {
            link = await this.processarCategoriaSubcategoria(link, pageNumber);
            pageNumber++;
          }
          if (!this.processo) { break; }
        }
        this.eans = [];
        return resolve(this.processo);
      } catch (error) {
        return reject(error);
      }
    });
  }

  private listarLinksCategoriaSubcategoria(link: string): Promise<string[]> {
    return new Promise<string[]>(async (resolve, reject) => {
      try {
        let $ = await this.load(link);
        let links = await this.listarLinks($, '.rnk-n3 > a');
        links = links.filter((item, pos, self) => 
          !item || (item.split('/').length == 4 && self.indexOf(item) == pos)
        );
        links.push('/nossas-marcas-exclusivas');
        return resolve(links);
      } catch (error) {
        return reject(error);
      }
    });
  }

  private processarCategoriaSubcategoria(link: string, pageNumber: number = 1): Promise<string> {
    return new Promise<string>(async (resolve, reject) => {
      if (global.gc) global.gc(); // NODE_OPTIONS=--expose-gc
      
      if (link.indexOf('buscavazia') != -1) {
        return resolve(null);
      }

      let path = link;
      let subcategoria;
      let categoria;
      let sessao;
      if ((path.match(/\//g) || []).length == 3) {
        subcategoria = path.substring(path.lastIndexOf("/") + 1, path.length);
        path = path.substring(0, path.lastIndexOf("/"));
        categoria = path.substring(path.lastIndexOf("/") + 1, path.length);
        path = path.substring(0, path.lastIndexOf("/"));
        sessao = path.substring(path.lastIndexOf("/") + 1, path.length);
      } else {
        sessao = 'MARCA EXCLUSIVA';
      }
      
      let $: CheerioStatic = null;
      let produtos: Produto[] = [];

      let attemptCount = 0;
      while (true) {
        try {
          $ = await this.load(`${PachecoExtraction.SITE + link}?PS=50&PageNumber=${pageNumber}`);

          if ($('h1').length > 0) {
            return resolve(null);
          }

          const linksProdutos = await this.listarLinks($, '.collection-image-link');
          produtos = await this.listarProdutos(linksProdutos, sessao, categoria, subcategoria);
          break;
        } catch (error) {
          if (attemptCount++ >= 10) return reject(error)
          await this.sleep(60000);
        }
      } 

      try {
        this.processo = await this.processoService.obter({ id: this.processo.id });
        if (this.processo && produtos.length > 0) {
          produtos = await this.produtoService.salvarTodos(produtos);
          this.processo.totalProcessado += produtos.length; // Parcial
          await this.processoService.salvar(this.processo);
        } else {
          return resolve(null);
        }
        
        return resolve(link);
      } catch (error) {
        return reject(error);
      }
    });
  }

  private listarProdutos(linksProdutos: string[], sessao:string, categoria: string, subcategoria: string): Promise<Produto[]> {
    return new Promise<Produto[]>(async (resolve, reject) => {
      try {
        const produtos: Produto[] = [];
        while (produtos.length < linksProdutos.length) {
          if (global.gc) global.gc();

          const produtoPromisses: Promise<Produto>[] = [];
          for(let i = produtos.length; i < produtos.length + linksProdutos.length; i++) {
            if (i == linksProdutos.length) break;
            produtoPromisses.push(this.extrairProduto(linksProdutos[i], sessao, categoria, subcategoria).catch( err => { throw err }));
          }
  
          produtos.push( ...await Promise.all(produtoPromisses) ) ;
        }

        for(const produto of produtos) {
          produto.analise = this.analise;
          produto.processo = this.processo;
          produto.concorrente = Concorrente.Pacheco;
        }

        return resolve(produtos.filter(produto => produto.ean));
      } catch (error) {
        return reject(error);
      }
    });
  }

  private extrairProduto(link: string, sessao:string, categoria: string, subcategoria: string): Promise<Produto> {
    return new Promise<Produto>(async (resolve, reject) => {
      const produto = new Produto();

      try {
        let $ = await this.load(link);

        const produto = new Produto;
        
        let descricaoDiv = $('div.productDescription');
        produto.descricao = descricaoDiv ? descricaoDiv.text() : null;
        if (produto.descricao && produto.descricao.length > 4000) produto.descricao = produto.descricao.substring(0, 4000);
        produto.sessao = sessao ? sessao.toUpperCase().replace('-', ' ') : '';
        produto.categoria = categoria ? categoria.toUpperCase().replace('-', ' ') : '';
        produto.subcategoria = subcategoria ? subcategoria.toUpperCase().replace('-', ' ') : '';
        
        let produtoJson:any = null;
        let totalScripts = $('script').length;
        for (let i = 0; i < totalScripts; i++) {
          produtoJson = $('script')[i];
          if (produtoJson.firstChild && produtoJson.firstChild.data.indexOf('vtex.events.addData') != -1) {
            break;
          }
        }
        
        produtoJson = produtoJson.firstChild.data;
        produtoJson = produtoJson.substring(produtoJson.indexOf('{'), produtoJson.indexOf(');'));
        produtoJson = JSON.parse(produtoJson);

        produto.ean = produtoJson.productEans[0];
        if (this.eans && this.eans.filter((prod: any) => prod === produto.ean + produto.sessao + produto.categoria + produto.subcategoria).length > 0) {
          produto.ean = undefined;
          return resolve(produto);
        }
        if (this.eans) this.eans.push(produto.ean + produto.sessao + produto.categoria + produto.subcategoria);

        produto.sku = produtoJson.productId;
        produto.nome = produtoJson.productName ? produtoJson.productName.toUpperCase() : '';
        produto.precoRegular = produtoJson.productListPriceFrom;
        produto.precoFinal = produtoJson.productPriceTo;
        produto.marca = produtoJson.productBrandName.toUpperCase();
        produto.link = link;
        produto.ultimaAtualizacao = new Date();

        let vantagensDiv = $('div.rnk-flags');
        produto.vantagens = vantagensDiv ? vantagensDiv.text() : null;
        if (produto.vantagens) {
          while (produto.vantagens.indexOf("  ") >= 0) {
            produto.vantagens = produto.vantagens.replace("  ", " ");
          }
          produto.vantagens = produto.vantagens.trim().toUpperCase();
        }
        
        /*$ = await this.load(`https://drogariaspacheco.vteximg.com.br/produto/sku/${produto.sku}`)*/

        return resolve(produto);
      } catch (error) {
        return resolve(produto);
      }
    });
  }

  private listarLinks($: CheerioStatic, selector: string): Promise<string[]> {
    return new Promise<string[]>(async (resolve, reject) => {
      try {
        const links: string[] = []
        $(selector).each( (i, e) => {
          links.push(e.attribs['href']);
        });
        return resolve(links);
      } catch (error) {
        return reject(error);
      }
    });
  }

  private load(link: string): Promise<CheerioStatic> {
    return new Promise<CheerioStatic>(async (resolve, reject) => {
      try {
        request(link, (err, res, body) => {
          if (err) return reject(new Error(err));
          return resolve (cheerio.load(body));           
        })
      } catch (error) {
        return reject(error);
      }
    });
  }

  private sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  
  async atualizarProdutos(produtos: Produto []): Promise<Produto []> {
    let produtosAtualizados = [];
    for(let produto of produtos) {
      let produtoTemp = await this.extrairProduto(produto.link, produto.sessao, produto.categoria, produto.subcategoria);
      produtoTemp.id = produto.id;
      produtoTemp.analise = produto.analise;
      produtoTemp.processo = produto.processo;
      produtoTemp.concorrente = produto.concorrente;
      produtoTemp.sessao = produto.sessao;
      produtoTemp.categoria = produto.categoria;
      produtoTemp.subcategoria = produto.subcategoria;
      produtosAtualizados.push(produtoTemp);
    }
    return await this.produtoService.salvarTodos(produtosAtualizados);
  }

  async conciliarProdutos(processo: Processo, EANs: string[]): Promise<Produto[]> {
    return new Promise(async (resolve, reject) => {
      try {
              
        // TODO

        return reject(new Error(`Processo de conciliar produtos não implementado!`));
      } catch (error) {
        return reject(error);
      }
    });
  }
}
