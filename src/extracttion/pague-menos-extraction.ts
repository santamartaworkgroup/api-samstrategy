import cheerio from 'cheerio';
import { Browser, Page } from 'puppeteer';
import request from 'request';
import { Inject, Service } from 'typedi';
import { Processo } from '../domain/entity/processo';
import { ProcessoService } from '../domain/service/processo-service';
import { ProdutoService } from '../domain/service/produto-service';
import { Analise } from './../domain/entity/analise';
import { Produto } from './../domain/entity/produto';
import { Concorrente } from './../domain/entity/type/concorrente';
import { ConcorrenteExtracttion } from './concorrente-extraction';




const WAIT_FOR_SELECTOR = 5000;
const LAUNCH_OPTIONS = { headless: true, devtools: false, args: ['--no-sandbox', '--disable-setuid-sandbox'] };
@Service()
export class PagueMenosExtraction implements ConcorrenteExtracttion {
  private static SITE = 'https://www.paguemenos.com.br';

  @Inject('produtoService')
  produtoService: ProdutoService;

  @Inject('processoService')
  processoService: ProcessoService;

  private processo: Processo;
  private analise: Analise;

  private browser: Browser;

  extrairProdutos(processo: Processo): Promise<Processo> {
    return new Promise(async (resolve, reject) => {
      this.processo = processo;
      this.analise = processo.analise;
      try { 
        let links = [];
        links = await this.listarLinksCategoriaSubcategoria(`${PagueMenosExtraction.SITE}`);
        links.push(`/landing-page-ame`);
        
        for(let link of links) {
          let pageNumber = 1;
          while (link) {
            link = await this.processarCategoriaSubcategoria(link, pageNumber);
            pageNumber++;
          }
          if (!this.processo) { break; }
        }
      
        if (this.browser) this.browser.close();

        return resolve(this.processo);
      } catch (error) {
        if (this.browser) this.browser.close();
        return reject(error);
      }
    });
  }

  private listarLinksCategoriaSubcategoria(link: string): Promise<string[]> {
    return new Promise<string[]>(async (resolve, reject) => {
      try {
        let $ = await this.load(link);
        const links = await this.listarLinks($);
        return resolve(links);
      } catch (error) {
        return reject(error);
      }
    });
  }

  private processarCategoriaSubcategoria(link: string, pageNumber: number = 1): Promise<string> {
    return new Promise<string>(async (resolve, reject) => {
      if (global.gc) global.gc(); // NODE_OPTIONS=--expose-gc

      let produtos: Produto[] = [];
      let $: CheerioStatic = null;

      let attemptCount = 0;
      while (true) {
        try {
          $ = await this.load(`${PagueMenosExtraction.SITE + link}?page=${pageNumber}`);

          let idsProdutosScript:any = null;
          let totalScripts = $('script').length;
          for (let i = 0; i < totalScripts; i++) {
            idsProdutosScript = $('script')[i];
            if (idsProdutosScript.firstChild && idsProdutosScript.firstChild.data.indexOf('__RUNTIME__') != -1) {
              break;
            }
          }
          idsProdutosScript = idsProdutosScript.firstChild.data.split('productId\":\"');
          let idsProdutos = [];
          for (let i = 1; i < idsProdutosScript.length; i++) {
            idsProdutos.push(idsProdutosScript[i].substring(0, idsProdutosScript[i].indexOf('\"')));
          }
          if (idsProdutos.length == 0) {
            return resolve(null);
          }
          let breadcrumb = $('.vtex-breadcrumb-1-x-link.vtex-breadcrumb-1-x-link--1');
          let sessao = breadcrumb && breadcrumb.length > 0 ? breadcrumb[0].firstChild.data : undefined;

          produtos = await this.listarProdutosPorIds(idsProdutos, sessao, '', '');
          
          /*
          this.browser = this.browser ? this.browser : await puppeteer.launch( LAUNCH_OPTIONS );
          const page = await this.newPage(this.browser, `${PagueMenosExtraction.SITE + link}?page=${pageNumber}`);

          const linksProdutos = await this.listarLinksProdutosPuppeteer(page, '.vtex-product-summary-2-x-clearLink');
          page.removeAllListeners('response');
          page.close();
          //const linksProdutos = await this.listarLinksProdutos($, '.vtex-product-summary-2-x-clearLink');
          if (linksProdutos.length == 0) {
            if (attemptCount++ >= 10) {
              link = undefined;
              break;  
            }
            await this.sleep(30000);
            continue;
          }
          produtos = await this.listarProdutos(linksProdutos);*/
          
          break;
        } catch (error) {
          if (attemptCount++ >= 10) return reject(error)
          await this.sleep(30000);
        }
      } 

      try {
        this.processo = await this.processoService.obter({ id: this.processo.id });
        if (this.processo) {
          produtos = await this.produtoService.salvarTodos(produtos);
          this.processo.totalProcessado += produtos.length; // Parcial
          await this.processoService.salvar(this.processo);
        } else {
          return resolve(null);
        }
        
        return resolve(link);
      } catch (error) {
        console.log(error);
        return reject(error);
      }
    });
  }

  private listarLinksProdutosPuppeteer(page: Page, selector: string): Promise<string[]> {
    return new Promise<string[]>(async (resolve, reject) => {
      
      try {
        await page.waitForSelector(selector, { timeout: WAIT_FOR_SELECTOR });
      } catch (error) {
        return resolve([]);
      }

      try {
        const anchorHandleList = await page.$$(selector);
      
        const links = []
        for(const anchorHandle of anchorHandleList) {
          const link = await anchorHandle.evaluate( anchor => anchor.getAttribute("href")) as string;
          links.push(link);
        }

        return resolve(links);
      } catch (error) {
        return reject(error);
      }
    });
  }

  private newPage(browser: Browser, link: string): Promise<Page> {
    return new Promise<Page>(async (resolve, reject) => {
      try {
        const page = await browser.newPage();
        await page.goto(link, { timeout: 300000 });
        return resolve(page);
      } catch (error) {
        return reject(error);
      }
    });
  }

  private listarProdutosPorIds(idsProdutos: string[], sessao:string, categoria: string, subcategoria: string): Promise<Produto[]> {
    return new Promise<Produto[]>(async (resolve, reject) => {
      try {
        const produtos: Produto[] = [];
        while (produtos.length < idsProdutos.length) {
          if (global.gc) global.gc();

          const produtoPromisses: Promise<Produto>[] = [];
          for(let i = produtos.length; i < produtos.length + idsProdutos.length; i++) {
            if (i == idsProdutos.length) break;
            produtoPromisses.push(this.extrairProdutoPorId(idsProdutos[i]).catch( err => { throw err }));
          }
  
          produtos.push( ...await Promise.all(produtoPromisses) ) ;
        }

        for(const produto of produtos) {
          produto.analise = this.analise;
          produto.processo = this.processo;
          produto.concorrente = Concorrente.PagueMenos;
          produto.sessao = sessao.toUpperCase();
          produto.categoria = categoria.toUpperCase();
          produto.subcategoria = subcategoria.toUpperCase();
        }

        return resolve(produtos.filter(produto => produto.ean));
      } catch (error) {
        return reject(error);
      }
    });
  }

  private extrairProdutoPorId(idProduto: string): Promise<Produto> {
    return new Promise<Produto>(async (resolve, reject) => {
      const produto = new Produto();

      try {
        let produto = new Produto;
        let $ = await this.load(`https://paguemenos.vteximg.com.br/produto/sku/${idProduto}`)
        let produtoJson = JSON.parse($('body')[0].firstChild.data)[0];
        produto.ean = produtoJson.Ean;
        produto.nome = produtoJson.Name;
        produto.precoRegular = produtoJson.PriceWithoutDiscount;
        produto.precoFinal = produtoJson.Price;
        produto.peso = produtoJson.RealWeightKg;
        return resolve(produto);
      } catch (error) {
        return reject(produto);
      }
    });
  }

  private listarProdutos(linksProdutos: string[]): Promise<Produto[]> {
    return new Promise<Produto[]>(async (resolve, reject) => {
      try {
        const produtos: Produto[] = [];
        
        if (global.gc) global.gc();

        const produtoPromisses: Promise<Produto>[] = [];
        for(let i = produtos.length; i < produtos.length + linksProdutos.length; i++) {
          if (i == linksProdutos.length) break;
          produtoPromisses.push(this.extrairProduto(`${PagueMenosExtraction.SITE + linksProdutos[i]}`).catch( err => { throw err }));
        }

        produtos.push( ...await Promise.all(produtoPromisses) ) ;

        for(const produto of produtos) {
          produto.analise = this.analise;
          produto.processo = this.processo;
          produto.concorrente = Concorrente.PagueMenos;
        }

        return resolve(produtos.filter(produto => produto.ean));
      } catch (error) {
        console.log(error);
        return reject(error);
      }
    });
  }

  private extrairProduto(link: string): Promise<Produto> {
    return new Promise<Produto>(async (resolve, reject) => {
      let attemptCount = 0;
      while (true) {
        try {
          let $ = await this.load(link);

          let produto = new Produto;
          let idProduto: any = $('.vtex-product-identifier-0-x-product-identifier__value');
          if (!idProduto || idProduto.length == 0) {
            return resolve(produto);
          }
          idProduto = $('.vtex-product-identifier-0-x-product-identifier__value')[0].firstChild.data;
          let descricao: any = $('.vtex-store-components-3-x-productDescriptionText > div');
          descricao = descricao && descricao.length > 0 ? descricao[0].firstChild.data : '';
          let breadcrumb = $('.vtex-breadcrumb-1-x-link');
          let sessao = breadcrumb && breadcrumb.length > 1 ? breadcrumb[1].firstChild.data : undefined;
          let categoria = breadcrumb && breadcrumb.length > 2 ? breadcrumb[2].firstChild.data : undefined;

          $ = await this.load(`https://paguemenos.vteximg.com.br/produto/sku/${idProduto}`)
          let produtoJson = JSON.parse($('body')[0].firstChild.data)[0];
          produto.ean = produtoJson.Ean;
          produto.sessao = sessao;
          produto.categoria = categoria;
          produto.nome = produtoJson.Name;
          produto.descricao = descricao && descricao.length > 4000 ? descricao.substring(0, 4000) : descricao;
          produto.precoRegular = produtoJson.PriceWithoutDiscount;
          produto.precoFinal = produtoJson.Price;
          produto.peso = produtoJson.RealWeightKg;
          produto.link = link;
          produto.ultimaAtualizacao = new Date();
          return resolve(produto);
        } catch (error) {
          console.log(error);
          if (attemptCount++ >= 10)
              return reject(error);
          await this.sleep(30000);
        }
      }
    });
  }

  private listarLinks($: CheerioStatic): Promise<string[]> {
    return new Promise<string[]>(async (resolve, reject) => {
      try {
        const links: string[] = []
        $('a').each( (i, e) => {
          if (e.attribs['title'] && e.attribs['title'].indexOf('Ver todos') != -1) {
            links.push(e.attribs['href']);
          }
        });
        return resolve(links);
      } catch (error) {
        return reject(error);
      }
    });
  }

  private listarLinksProdutos($: CheerioStatic, selector: string): Promise<string[]> {
    return new Promise<string[]>(async (resolve, reject) => {
      try {
        const links: string[] = []
        $(selector).each( (i, e) => {
          links.push(e.attribs['href']);
        });
        return resolve(links);
      } catch (error) {
        return reject(error);
      }
    });
  }

  private load(link: string): Promise<CheerioStatic> {
    return new Promise<CheerioStatic>(async (resolve, reject) => {
      try {
        request(link, (err, res, body) => {
          if (err) return reject(new Error(err));
          return resolve (cheerio.load(body));           
        })
      } catch (error) {
        return reject(error);
      }
    });
  }

  private sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  async atualizarProdutos(produtos: Produto []): Promise<Produto []> {
    let produtosAtualizados = [];
    for(let produto of produtos) {
      let produtoTemp = await this.extrairProduto(produto.link);
      produtoTemp.id = produto.id;
      produtoTemp.analise = produto.analise;
      produtoTemp.processo = produto.processo;
      produtoTemp.concorrente = produto.concorrente;
      produtoTemp.sessao = produto.sessao;
      produtoTemp.categoria = produto.categoria;
      produtoTemp.subcategoria = produto.subcategoria;
      produtosAtualizados.push(produtoTemp);
    }
    return await this.produtoService.salvarTodos(produtosAtualizados);
  }

  async conciliarProdutos(processo: Processo, EANs: string[]): Promise<Produto[]> {
    return new Promise(async (resolve, reject) => {
      try {
              
        // TODO

        return reject(new Error(`Processo de conciliar produtos não implementado!`));
      } catch (error) {
        return reject(error);
      }
    });
  }
}
