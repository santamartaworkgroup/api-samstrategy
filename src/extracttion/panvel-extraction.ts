import { Inject, Service } from 'typedi';
import { Processo } from '../domain/entity/processo';
import { ProcessoService } from '../domain/service/processo-service';
import { ProdutoService } from '../domain/service/produto-service';
import { Produto } from './../domain/entity/produto';
import { ConcorrenteExtracttion } from './concorrente-extraction';



@Service()
export class PanvelExtraction implements ConcorrenteExtracttion {

  @Inject('produtoService')
  produtoService: ProdutoService;

  @Inject('processoService')
  processoService: ProcessoService;

  private processo: Processo;

  extrairProdutos(processo: Processo): Promise<Processo> {
    return new Promise(async (resolve, reject) => {
      try {
              
        // TODO

        return reject(new Error(`Processo de extração ${processo.concorrente} não implementado!`));
      } catch (error) {
        return reject(error);
      }
    });
  }

  async atualizarProdutos(produtos: Produto []): Promise<Produto []> {
    return new Promise(async (resolve, reject) => {
      try {
              
        // TODO

        return reject(new Error(`Processo de atualizar produtos não implementado!`));
      } catch (error) {
        return reject(error);
      }
    });
  }

  async conciliarProdutos(processo: Processo, EANs: string[]): Promise<Produto[]> {
    return new Promise(async (resolve, reject) => {
      try {
              
        // TODO

        return reject(new Error(`Processo de conciliar produtos não implementado!`));
      } catch (error) {
        return reject(error);
      }
    });
  }
}
