import http from "http";
import { Inject, Service } from "typedi";
import { Processo } from "../domain/entity/processo";
import { Produto } from "../domain/entity/produto";
import { Concorrente } from "../domain/entity/type/concorrente";
import { ProcessoService } from "../domain/service/processo-service";
import { ProdutoService } from "../domain/service/produto-service";
import { ConcorrenteExtracttion } from "./concorrente-extraction";




@Service()
export class SantaMartaExtraction implements ConcorrenteExtracttion {
  @Inject("produtoService")
  produtoService: ProdutoService;

  @Inject("processoService")
  processoService: ProcessoService;

  async extrairProdutos(processo: Processo): Promise<Processo> {
    return new Promise(async (resolve, reject) => {
      try {
        const analise = processo.analise;

        let page = 0;
        const size = 1000;

        while (true) {
          const lista = await this.getLista(page, size);

          if (lista.length) {
            let produtos: Produto[] = [];
            for (const produtoOrigem of lista) {
              const produtoDestino = new Produto();
              produtoDestino.analise = analise;
              produtoDestino.processo = processo;
              produtoDestino.concorrente = Concorrente.SantaMarta;

              this.copiar(produtoOrigem, produtoDestino);

              produtos.push(produtoDestino);
            }

            processo = await this.processoService.obter({ id: processo.id });
            if (processo) {
              produtos = await this.produtoService.salvarTodos(produtos);
              processo.totalProcessado += produtos.length; // Parcial
              await this.processoService.salvar(processo);
            } else {
              break;
            }

            page++;
            continue;
          }
          break;
        }

        return resolve(processo);
      } catch (error) {
        return reject(error);
      }
    });
  }

  getLista(page: number, size: number) {
    return new Promise<[]>((resolve, reject) => {
      http.get(`http://www2.samconecta.com.br:1921/samrearguard/produto-analise-concorrencia/listar-produtos?size=${size}&page=${page}&sort=id`, (response) => {
        let chunks_of_data: any[] = [];
  
        response.on('data', (fragments) => {
          chunks_of_data.push(fragments);
        });
  
        response.on('end', () => {
          let response_body = Buffer.concat(chunks_of_data);
          resolve(JSON.parse(response_body.toString()).content);
        });
  
        response.on('error', (error) => {
          reject(error);
        });
      });
    });
  }

  async atualizarProdutos(produtos: Produto []): Promise<Produto []> {
    return new Promise(async (resolve, reject) => {
      return resolve(produtos);
    });
  }

  private copiar(produtoOrigem: any, produtoDestino: Produto) {
    produtoDestino.sessao = produtoOrigem.sessao ? (produtoOrigem.sessao as string).toUpperCase() : null;
    produtoDestino.categoria = produtoOrigem.categoria ? (produtoOrigem.categoria as string).toUpperCase() : null;
    produtoDestino.subcategoria = produtoOrigem.subcategoria ? (produtoOrigem.subcategoria as string).toUpperCase() : null;
    produtoDestino.sku = produtoOrigem.sku;
    produtoDestino.nome = (produtoOrigem.nome as string).toUpperCase();
    produtoDestino.descricao = produtoOrigem.descricao;
    produtoDestino.ean = produtoOrigem.ean;
    produtoDestino.peso = produtoOrigem.peso;
    produtoDestino.precoRegular = produtoOrigem.precoRegular;
    produtoDestino.precoFinal = produtoOrigem.precoFinal;
    produtoDestino.quantidade = produtoOrigem.quantidade;
    produtoDestino.vantagens = produtoOrigem.vantagens ? (produtoOrigem.vantagens as string).toUpperCase() : null;
    produtoDestino.marca = produtoOrigem.marca ? (produtoOrigem.marca as string).toUpperCase() : null;
    produtoDestino.fabricante = produtoOrigem.fabricante ? (produtoOrigem.fabricante as string).toUpperCase() : null;
    produtoDestino.ultimaAtualizacao = new Date();
  }

  async conciliarProdutos(processo: Processo, EANs: string[]): Promise<Produto[]> {
    return new Promise(async (resolve, reject) => {
      try {
              
        // TODO

        return reject(new Error(`Processo de conciliar produtos não implementado!`));
      } catch (error) {
        return reject(error);
      }
    });
  }
}
