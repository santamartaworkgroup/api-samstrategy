import { ObjectType, Field, InputType } from "type-graphql";

import { BaseEntity } from "./base-entity";

@ObjectType({ isAbstract: true })
@InputType({ isAbstract: true })
export abstract class BaseEntityImpl<E extends BaseEntity<E, K>, K> implements BaseEntity<E, K> {
    
    id: K;

}