export interface BaseEntity<E extends BaseEntity<E, K>, K> {

    id: K;
    
}