import { InjectManager } from 'typeorm-typedi-extensions';
import { EntityManager } from "typeorm";

import { BaseRepository } from "./base-repository";
import { BaseEntity } from "../entity/base-entity";
import { ClassType } from "type-graphql";

export function BaseRepositoryImpl<E extends BaseEntity<E, K>, K>( entityClass: ClassType<E> ) {
  
  abstract class BaseRepositoryImplClass<E extends BaseEntity<E, K>, K> implements BaseRepository<E, K> {
    @InjectManager() protected manager: EntityManager;
  
    criar(dados?: any): E {
      return this.manager.create<E>(entityClass, dados) as E;
    }
  
    //
    // Mutation
    //
  
    salvar(entidade: E): Promise<E> {
      return this.manager.save<E>(entidade);
    }
  
    salvarTodos(entidades: E[]): Promise<E[]> {
      return this.manager.save<E[]>(entidades);
    }
  
    remover(entidade: E): Promise<E> {
      return this.manager.remove<E>(entidade);
    }
  
    removerTodos(entidades: E[]): Promise<E[]> {
      return this.manager.remove<E[]>(entidades);
    }

    incluir(dados: any): Promise<K> {
      return this.manager
        .insert<E>(entityClass, dados)
        .then((ir) => ir.identifiers[0].id);
    }
  
    incluirTodos(dados: any[]): Promise<K[]> {
      return this.manager
        .insert<E>(entityClass, dados)
        .then((ir) => ir.identifiers.map( item => item.id)
      );
    }

    alterar(dados: any, criterio: any): Promise<number> {
      return this.manager
        .update<E>(entityClass, criterio, dados)
        .then((ur) => Number(ur ? ur.affected : 0));
    }
    
    alterarPorIds(dados: any, ids: K[]): Promise<number> {
      return this.manager
        .update<E>(entityClass, ids, dados)
        .then((ur) => Number(ur ? ur.affected : 0));
    }
  
    deletar(criterio: any): Promise<number> {
      return this.manager
        .delete(entityClass, criterio)
        .then((dr) => Number(dr ? dr.affected : 0));
    }
  
    deletarPorIds(ids: K[]): Promise<number> {
      return this.manager
        .delete(entityClass, ids)
        .then((dr) => Number(dr ? dr.affected : 0));
    }
  
    //
    // Query
    //
  
    contagem(criterio?: any): Promise<number> {
      return this.manager.count(entityClass, criterio).then(count => Number(count));
    }
  
    carregar(dados: any): Promise<E | undefined> {
      return this.manager.preload<E>(entityClass, dados);
    }
  
    obter(criterio: any): Promise<E | undefined> {
      return this.manager.findOne<E>(entityClass, criterio);
    }
  
    listar(criterio?: any): Promise<[E[], number]> {
      return this.manager.findAndCount<E>(entityClass, criterio).then( result => [ result[0], result[1] ] );
    }
  
    listarPorIds(ids: K[]): Promise<E[]> {
      return this.manager.findByIds<E>(entityClass, ids);
    }
  }

  return BaseRepositoryImplClass;
}
