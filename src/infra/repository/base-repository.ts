import { BaseEntity } from './../entity/base-entity';

export interface BaseRepository<E extends BaseEntity<E, K>, K> {

  criar(dados?: any): E;

  //
  // Mutation
  //

  salvar(entidade: E): Promise<E>;
  salvarTodos(entidades: E[]): Promise<E[]>;

  remover(entidade: E): Promise<E>;
  removerTodos(entidades: E[]): Promise<E[]>;

  incluir(dados: any): Promise<K>;
  incluirTodos(dados: any[]): Promise<K[]>;

  alterar(dados: any, criterio: any): Promise<number>;
  alterarPorIds(dados: any, ids: K[]): Promise<number>;

  deletar(criterio: any): Promise<number>;
  deletarPorIds(ids: K[]): Promise<number>;

  //
  // Query
  //

  contagem(criterio?: any): Promise<number>;
  carregar(dados: any): Promise<E | undefined>;
  obter(criterio?: any): Promise<E | undefined>;
  listar(criterio?: any): Promise<[E[], number]>;
  listarPorIds(ids: K[]): Promise<E[]>;
  
}