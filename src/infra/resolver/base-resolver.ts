import { Resolver, Query, Mutation, Arg, ClassType, Float, ObjectType, Int, Field } from 'type-graphql';

import { BaseRepository } from './../repository/base-repository';
import { BaseEntity } from '../entity/base-entity';
import { BaseService } from '../service/base-service';
import { type } from 'os';

export interface ListAndCountInterface<E extends BaseEntity<E, any>> {
  list: E[];
  count: number;
}

export function ListAndCount<E extends BaseEntity<E, any>>( entityClass: ClassType<E> ) {

  @ObjectType({ isAbstract: true })
  abstract class ListAndCountClass<E extends BaseEntity<E, any>> implements ListAndCountInterface<E> {
    @Field( type => [entityClass]) list: E[];
    @Field( type => Int! ) count: number;

    constructor(list: E[], count: number) {
        this.list = list;
        this.count = count;
    }
  }

  return ListAndCountClass;
}

export function BaseResolver<E extends BaseEntity<E, K>, K, LC extends ListAndCountInterface<E>>( entityClass: ClassType<E>, keyClass: ClassType<K>, lcClass: ClassType<LC>) {

  const _resourceName = entityClass.name;

  @Resolver(of => entityClass, { isAbstract: true })
  abstract class BaseResolverClass<S extends BaseService<R, E, K>, R extends BaseRepository<E, K>, E extends BaseEntity<E, K>, K> {
    
    static resourceName = _resourceName;

    constructor(protected service: S) {}
  
    //
    // Mutation
    //

    @Mutation(returns => entityClass, { name: `${_resourceName}_salvar`, description: `Salvar um(a) ${_resourceName}` })
    async salvar(@Arg('entidade', type => entityClass) entidade: E) {
      return await this.service.salvar(entidade);
    }
  
    @Mutation(returns => [entityClass], { name: `${_resourceName}_salvarTodos`, description: `Salvar um(a) lista de ${_resourceName}` })
    async salvarTodos(@Arg('entidades', type => [entityClass]) entidades: E[]) {
      return await this.service.salvarTodos(entidades);
    }

    @Mutation(returns => entityClass, { name: `${_resourceName}_remover`, description: `Remover um(a) ${_resourceName}` })
    async remover(@Arg('entidade', type => entityClass) entidade: E) {
      return await this.service.remover(entidade);
    }

    @Mutation(returns => [entityClass], { name: `${_resourceName}_removerTodos`, description: `Remover um(a) lista de ${_resourceName}` })
    async removerTodos(@Arg('entidades', type => [entityClass]) entidades: E[]) {
      return await this.service.removerTodos(entidades);
    }

    @Mutation(returns => keyClass, { name: `${_resourceName}_incluir`, description: `Incluir um(a) ${_resourceName}` })
    async incluir(@Arg('dados', type => entityClass) dados: any) {
      return await this.service.incluir(dados);
    }

    @Mutation(returns => [keyClass], { name: `${_resourceName}_incluirTodos`, description: `Incluir um(a) lista de ${_resourceName}` })
    async incluirTodos(@Arg('dados', type => [entityClass]) dados: any[]) {
      return await this.service.incluirTodos(dados);
    }

    @Mutation(returns => Int, { name: `${_resourceName}_alterar`, description: `Alterar campos por critério ${_resourceName}` })
    async alterar(@Arg('dados', type => entityClass) dados: any, @Arg('criterio', type => String) criterio: string) {
      return await this.service.alterar(dados, JSON.parse(criterio));
    }

    @Mutation(returns => Int, { name: `${_resourceName}_alterarPorIds`, description: `Alterar campos por ids ${_resourceName}` })
    async alterarPorIds(@Arg('dados', type => entityClass) dados: any, @Arg('ids', type => [keyClass]) ids: K[]) {
      return await this.service.alterarPorIds(dados, ids);
    }

    @Mutation(returns => Int, { name: `${_resourceName}_deletar`, description: `Deletar ${_resourceName} por critério` })
    async deletar(@Arg('criterio', type => String) criterio: string) {
      return await this.service.deletar(JSON.parse(criterio));
    }
  
    @Mutation(returns => Int, { name: `${_resourceName}_deletarPorIds`, description: `Deletar ${_resourceName} por ids` })
    async deletarPorIds(@Arg('ids', type => [keyClass]) ids: K[]) {
      return await this.service.deletarPorIds(ids);
    }

    //
    // Query
    //

    @Query(returns => Int, { name: `${_resourceName}_contagem`, description: `Contar ${_resourceName} por critério` })
    async contagem(@Arg('criterio', type => String, { nullable: true }) criterio?: string) {
      return await this.service.contagem(criterio ? JSON.parse(criterio) : null);
    }
  
    @Query(returns => entityClass, { name: `${_resourceName}_obter`, description: `Obter ${_resourceName} por critério` })
    async obter(@Arg('criterio', type => String, { nullable: true }) criterio?: string) {
      return await this.service.obter(criterio ? JSON.parse(criterio) : null);
    }

    @Query(returns => lcClass, { name: `${_resourceName}_listar`, description: `Listar ${_resourceName} por critério` })
    async listar(@Arg('criterio', type => String, { nullable: true }) criterio?: string) {
      return await this.service.listar(criterio ? JSON.parse(criterio) : null).then( r => new lcClass(r[0], r[1]) );
    }
  
    @Query(returns => [entityClass], { name: `${_resourceName}_listarPorIds`, description: `Listar ${_resourceName} por ids` })
    async listarPorIds(@Arg('ids', type => [keyClass]) ids: K[]) {
      return await this.service.listarPorIds(ids);
    }
  }

  return BaseResolverClass;
}

