import { Service } from "typedi";

import { BaseEntity } from "../entity/base-entity";
import { BaseRepository } from "./../repository/base-repository";
import { BaseService } from "./base-service";

@Service()
export abstract class BaseServiceImpl<R extends BaseRepository<E, K>, E extends BaseEntity<E, K>, K> implements BaseService<R, E, K> {

  constructor(protected repository: R) {}

  criar(dados?: any): E {
    return this.repository.criar(dados);
  }

  //
  // Mutation
  //

  salvar(entidade: E): Promise<E> {
    return this.repository.salvar(entidade);
  }

  salvarTodos(entidades: E[]): Promise<E[]> {
    return this.repository.salvarTodos(entidades);
  }

  remover(entidade: E): Promise<E> {
    return this.repository.remover(entidade);
  }

  removerTodos(entidades: E[]): Promise<E[]> {
    return this.repository.removerTodos(entidades);
  }

  incluir(dados: any): Promise<K> {
    return this.repository.incluir(dados);
  }

  incluirTodos(dados: any[]): Promise<K[]> {
    return this.repository.incluirTodos(dados);
  }

  alterar(dados: any, criterio: any): Promise<number> {
    return this.repository.alterar(dados, criterio);
  }

  alterarPorIds(dados: any, ids: K[]): Promise<number> {
    return this.repository.alterarPorIds(dados, ids);
  }

  deletar(criterio: any): Promise<number> {
    return this.repository.deletar(criterio);
  }

  deletarPorIds(ids: K[]): Promise<number> {
    return this.repository.deletarPorIds(ids);
  }

  //
  // Query
  //

  contagem(criterio?: any): Promise<number> {
    return this.repository.contagem(criterio);
  }

  carregar(dados: any): Promise<E | undefined> {
    return this.repository.carregar(dados);
  }

  obter(criterio?: any): Promise<E | undefined> {
    return this.repository.obter(criterio);
  }

  listar(criterio?: any): Promise<[E[], number]> {
    return this.repository.listar(criterio);
  }

  listarPorIds(ids: K[]): Promise<E[]> {
    return this.repository.listarPorIds(ids);
  }
}
