import { BaseEntity } from './../entity/base-entity';
import { BaseRepository } from './../repository/base-repository';

export interface BaseService<R extends BaseRepository<E, K>, E extends BaseEntity<E, K>, K> extends BaseRepository<E, K> {

}