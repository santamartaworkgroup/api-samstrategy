import dotenv from 'dotenv';

dotenv.config();

import { Md5 } from 'md5-typescript';
import "reflect-metadata";

import { createConnection, useContainer } from "typeorm";

import { Container } from "typedi";

useContainer(Container);

import express  from "express";
import cors from 'cors';

import { ApolloServer } from "apollo-server-express";
import { buildSchema } from "type-graphql";

import { resolvers } from './resolver';

import { registerEntities } from './domain/entity';
import { registerRepositories } from "./domain/repository/impl";
import { registerServices } from "./domain/service/impl";

import { UsuarioService } from './domain/service/usuario-service';
import { ProcessoService } from './domain/service/processo-service';

async function main() {
  await createConnection();

  registerEntities();
  registerRepositories();
  registerServices();

  inicializar();

  start();
}

async function start() {
  const schema = await buildSchema({ resolvers: resolvers() as any, container: Container });
  const server = new ApolloServer({ schema });

  const app = express();

  app.options('*', cors());

  server.applyMiddleware({
    app, 
    path: '*',
    cors: true
  });
 
  app.listen({ port: 2220 }, () => {
    console.log(`Server running!`);
  });  
}

async function inicializar() {
  criarUsuarioAdmin();
  iniciarProcessosExtracao();
}

async function criarUsuarioAdmin() {
  const usuarioService = Container.get<UsuarioService>('usuarioService');

  if (await usuarioService.contagem() == 0) {
    let usuario = usuarioService.criar({ login: 'admin', senha: Md5.init('123'), nome: 'ADMINISTRADOR DO SISTEMA', email: 'emmanuel.ruiz@cognata.com.br' });
    usuario = await usuarioService.salvar(usuario);
  }
}

async function iniciarProcessosExtracao() {
  const processoService = Container.get<ProcessoService>('processoService');

  processoService.iniciarProcessos();
}  

main();
