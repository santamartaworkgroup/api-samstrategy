import { Concorrente } from './../domain/entity/type/concorrente';
import { Service, Inject } from 'typedi';
import { Resolver, Mutation, Arg, ObjectType, Field, Query } from 'type-graphql';
import { BaseResolver, ListAndCount } from '../infra/resolver/base-resolver';

import { AnaliseService } from '../domain/service/analise-service';
import { AnaliseRepository } from './../domain/repository/analise-repository';
import { Analise } from '../domain/entity/analise';
import { Any } from 'typeorm';

@ObjectType()
export class AnaliseListAndCount extends ListAndCount(Analise)<Analise> {}

@Resolver()
@Service()
export class AnaliseResolver extends BaseResolver(Analise, String, AnaliseListAndCount)<AnaliseService, AnaliseRepository, Analise, string> {
    
    constructor(
        @Inject('analiseService')
        service: AnaliseService
    ) {
        super( service );
    }

    @Mutation(returns => Boolean, { name: `${AnaliseResolver.resourceName}_conciliar`, description: `Conciliar produtos dos concorrentes` })
    async conciliar( @Arg('concorrentes', type => [String]) concorrentes: string[] ) {
      for(let i = 1; i < concorrentes.length; i++) {
        this.service.conciliar(concorrentes[0], concorrentes[i]);
      }
      return true;
    }

}