import { UsuarioResolver } from './usuario-resolver';
import { AnaliseResolver } from './analise-resolver';
import { ProcessoResolver } from './processo-resolver';
import { ProdutoResolver } from './produto-resolver';

export function resolvers(): any[] {
    return [
        UsuarioResolver,
        AnaliseResolver,
        ProcessoResolver,
        ProdutoResolver
    ];
}