import { Service } from 'typedi';
import { Inject } from 'typedi';
import { Resolver, ObjectType, Field } from 'type-graphql';
import { BaseResolver, ListAndCount } from '../infra/resolver/base-resolver';

import { ProcessoService } from '../domain/service/processo-service';
import { ProcessoRepository } from '../domain/repository/processo-repository';
import { Processo } from '../domain/entity/processo';

@ObjectType()
export class ProcessoListAndCount extends ListAndCount(Processo)<Processo> {}

@Resolver()
@Service()
export class ProcessoResolver extends BaseResolver(Processo, String, ProcessoListAndCount)<ProcessoService, ProcessoRepository, Processo, string> {
    
    constructor(
        @Inject('processoService')
        service: ProcessoService
    ) {
        super( service );
    }

}