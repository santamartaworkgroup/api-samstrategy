import { Service } from 'typedi';
import { Inject } from 'typedi';
import { Resolver, ObjectType, Field, Query, Arg } from 'type-graphql';
import { BaseResolver, ListAndCount } from '../infra/resolver/base-resolver';

import { ProdutoService } from '../domain/service/produto-service';
import { ProdutoRepository } from './../domain/repository/produto-repository';
import { Produto } from '../domain/entity/produto';

@ObjectType()
export class ProdutoListAndCount extends ListAndCount(Produto)<Produto> {}

@Resolver()
@Service()
export class ProdutoResolver extends BaseResolver(Produto, String, ProdutoListAndCount)<ProdutoService, ProdutoRepository, Produto, string> {
    
    constructor(
        @Inject('produtoService')
        service: ProdutoService
    ) {
        super( service );
    }

    @Query(returns => String, { name: `Produto_consultarConcorrencia`, description: `Consultar concorrência` })
    async consultarConcorrencia(@Arg('filtro', type => String, { nullable: true }) filtro: string) {
      return await this.service.consultarConcorrencia(filtro ? JSON.parse(filtro) : null).then(result => JSON.stringify(result));
    }

    @Query(returns => String, { name: `Produto_atualizarProdutos`, description: `Atualizar produtos` })
    async atualizarProdutos(@Arg('filtro', type => String, { nullable: true }) filtro: string) {
      return await this.service.atualizarProdutos(filtro ? JSON.parse(filtro) : null).then( () => true );
    }
    
}