import { Service, Inject } from 'typedi';
import { Resolver, Arg, Query, ObjectType, Field } from 'type-graphql';
import { BaseResolver, ListAndCount } from '../infra/resolver/base-resolver';

import { UsuarioService } from '../domain/service/usuario-service';
import { UsuarioRepository } from '../domain/repository/usuario-repository';
import { Usuario } from '../domain/entity/usuario';

@ObjectType()
export class UsuarioListAndCount extends ListAndCount(Usuario)<Usuario> {}

@Resolver()
@Service()
export class UsuarioResolver extends BaseResolver(Usuario, String, UsuarioListAndCount)<UsuarioService, UsuarioRepository, Usuario, string> {
    
    constructor(
        @Inject('usuarioService')
        service: UsuarioService
    ) {
        super( service );
    }
  
    @Query(returns => Usuario, { name: `${UsuarioResolver.resourceName}_autenticar`, description: `Autenticar login e senha do Usuário` })
    async autenticar(
        @Arg('login', type => String) login: string,
        @Arg('senha', type => String) senha: string
    ) {
      return await this.service.obter({ login, senha }).then( entidade => {
        if (entidade) return entidade;
        throw new Error('Usuário ou senha inválidos')
      });
    }

}